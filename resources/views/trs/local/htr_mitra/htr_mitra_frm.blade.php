@extends('layouts.coloradmin')
<!-- ------------------------------------------------------------------------------- -->
@section('title')Data Mitra & Penanam @stop
<!-- ------------------------------------------------------------------------------- -->
@section('title-small') @stop
<!-- ------------------------------------------------------------------------------- -->
@section('breadcrumb')
<span ng-show="f.tab=='list'">Data List</span>
<span ng-show="f.tab=='frm'">Form Entry</span> @stop
<!-- ------------------------------------------------------------------------------- -->
@section('content')
<div class="panel panel-success">
    <div class="panel-heading">
        @component('layouts.common.coloradmin.panel_button') @endcomponent @yield('breadcrumb')
    </div>
    <div class="panel-body">
        <div class="m-b-5 form-inline">
            <div class="pull-right">
                <div ng-show="f.tab=='list'">
                    @component('layouts.common.coloradmin.guide',['tag'=>'trs_local_htr_mitra']) @endcomponent
                    <div class="input-group">
                        <div class="btn-group">
                            <button type="button" class="btn btn-success btn-sm" onclick="SfExportExcel('div1')"><i class="fa fa fa-file-excel-o"></i></button>
                            <button type="button" class="btn btn-success btn-sm" ng-click="oPrint()"><i class="fa fa fa-print"></i></button>
                            <button type="button" class="btn btn-success btn-sm"  ng-click="oSearch(1)"><i class="fa fa fa-recycle"></i></button>
                        </div>
                    </div>
                    <div class="input-group">
                        <input type="text" class="form-control input-sm" ng-model="f.q" ng-enter="oSearch()" placeholder="Search">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-success btn-sm" ng-click="oSearch()"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
                <div ng-show="f.tab=='frm'">
                    <button type="button" class="btn btn-sm btn-success" ng-click="oSave()" ng-show="f.crud=='c' && f.trash!=1"><i class="fa fa-save"></i> Create</button>
                    <button type="button" class="btn btn-sm btn-success" ng-click="oSave()" ng-show="f.crud=='u' && f.trash!=1"><i class="fa fa-save"></i> Update</button>
                    <button type="button" class="btn btn-sm btn-warning" ng-click="oCopy()" ng-show="f.crud=='u'"><i class="fa fa-copy"></i> Copy</button>
                    <button type="button" class="btn btn-sm btn-danger" ng-click="oDel()" ng-show="f.crud=='u'&& f.trash!=1"><i class="fa fa-trash"></i> Delete</button>
                    <button type="button" class="btn btn-sm btn-warning" ng-click="oRestore()" ng-show="f.crud=='u' && f.trash==1"><i class="fa fa-recycle"></i> Restore</button>
                    <button type="button" class="btn btn-sm btn-info" ng-click="oLog()" ng-show="f.crud=='u'"><i class="fa fa-clock-o"></i> Log</button>
                    @component('layouts.common.coloradmin.upload') @endcomponent
                    
                </div>
            </div>
            <button type="button" class="btn btn-sm btn-inverse" ng-click="oNew()" ng-attr-title="Buat Baru" ng-show="f.tab=='list' && f.trash!=1"><i class="fa fa-plus"></i> New</button>
            <button type="button" class="btn btn-sm btn-inverse" ng-click="f.tab='list';m = []" ng-attr-title="Kembali ke Halaman Awal" ng-show="f.tab=='frm'"><i class="fa fa-arrow-left"></i> Back</button>
        </div>
        <br>
        <div ng-show="f.tab=='list'">
            <div class="alert alert-warning" ng-show="f.trash==1"><i class="fa fa-warning fa-2x"></i> This is deleted item<br>Trashed</div>
            <div id="div1" class="table-responsive">
                <table ng-table="tableList" show-filter="false" class="table table-condensed table-hover table-image" style="white-space: nowrap;">
                    <tr ng-repeat="v in $data">
                        <td title="'#'" filter="">
                            <button type="button" title="Edit" class="btn btn-xs btn-warning btn-flat" ng-click="oShow(v.idmitra)" style="margin-bottom: 5px;"><i class="fa fa-edit"></i></button><br/>
                            <button type="button" title="Pendaftaran" class="btn btn-xs btn-info btn-flat" ng-click="goToPendaftaran(v.idmitra)"><i class="fa fa-check"></i></button>
                        </td>
                        <td title="'Foto Profile'" class="p-5 text-center">      
                            <a href="{{\App\Sf::fileFtpAuthUrl('')}}/@{{v.foto}}" target="_blank">
                                <div class="" ng-hide="v.foto==null">
                                <img width="60px" ng-src="{{\App\Sf::fileFtpAuthUrl('')}}/@{{v.foto}}" alt="Foto @{{v.nama}}" onerror="this.src='<?=url('coloradmin/assets/img/no-pict.png')?>'">
                                </div>
                            </a>
                        </td>
                        <td title="'Nama'" filter="{nama: 'text'}" sortable="'nama'"><b class="text-primary">@{{v.nama}}</b>
                            <br/><b>idmitra : </b>@{{v.idmitra}} <a ng-show="@{{v.ktp}}!=null" class="pointer" href="{{\App\Sf::fileFtpAuthUrl('')}}/@{{v.ktp}}" target="_blank"><i class="fa fa-paperclip"> </i></a>                            
                            </td>
                        <td title="'Alamat'" filter="{alamat: 'text'}" sortable="'alamat'">@{{v.alamat}}
                            <br /><b>Kontak: </b><a href="https://api.whatsapp.com/send?phone=@{{v.no_wa.toString().replace('08','628')}}" target="_blank" title="Whatsapp to"> WA <i class="fa fa-comment"></i></a> @{{v.no_hp.toString().replace('08','628')}}
                            <br /><b>Email :</b><a href="mailto:@{{v.email}}">@{{v.email}}</a>
                        </td>
                        
                        
                    </tr>
                </table>
            </div>
        </div>
        <div ng-show="f.tab=='frm'">
            <form action="#" name="frm" id="frm">
                <h5 class="text-bold text-primary">Identitas Diri</h5>
                <div class="row">
                    <div class="col-sm-4">
                        <label title='idmitra'>ID MITRA</label>
                        <input type="text" ng-model="h.idmitra" id="h_idmitra" class="form-control input-sm" required ng-readonly="f.crud!='c'  "  >                        
                    </div>
                    <div class="col-sm-4">
                        <label title='nama'>Nama</label>
                        <input type="text" ng-model="h.nama" id="h_nama" class="form-control input-sm" required maxlength="100"  >
                        <label title='jenis_kelamin'>Jenis Kelamin</label>
                        <select ng-model="h.jenis_kelamin" id="h_jenis_kelamin" class="form-control input-sm" required>
                            <option ng-repeat="v in [['L','Laki-Laki'],['P','Perempuan']]" ng-value="v[0]">@{{v[1]}}</option>
                        </select>
                    </div>
                   
                </div>
                <hr/>
                <h5 class="text-bold text-primary">Data Alamat</h5>
                <div class="row">
                    <div class="col-sm-3">
                        <label title='provinsi'>Provinsi</label> : <span class="text-bold text-primary pointer" ng-click="h.provinsi = '';h.provinsi_name = ''">@{{ h.provinsi }}</span>
                        <input type="text" ng-model="h.provinsi_name" id="h_provinsi_name" class="form-control input-sm" required maxlength="100"  data-min-length="0" data-html="1" data-auto-select="true" data-animation="am-flip-x" bs-options="icon.value as icon.html for icon in getProvinsi($viewValue)" bs-typeahead bs-on-select="oSetProvinsi" ng-readonly="h.kabupaten">
                    </div>
                    <div class="col-sm-3">
                        <label title='kabupaten'>Kabupaten / Kota</label> : <span class="text-bold text-primary pointer" ng-click="h.kabupaten = '';h.kabupaten_name = ''">@{{ h.kabupaten }}</span>
                        <input type="text" ng-model="h.kabupaten_name" id="h_kab_name" class="form-control input-sm" required maxlength="100"  data-min-length="0" data-html="1" data-auto-select="true" data-animation="am-flip-x" bs-options="icon.value as icon.html for icon in getKab($viewValue)" bs-typeahead bs-on-select="oSetKab" ng-readonly="h.kecamatan">
                    </div>
                    <div class="col-sm-3">
                        <label title='kecamatan'>Kecamatan</label> : <span class="text-bold text-primary pointer" ng-click="h.kecamatan = '';h.kecamatan_name = ''">@{{ h.kecamatan }}</span>
                        <input type="text" ng-model="h.kecamatan_name" id="h_kec_name" class="form-control input-sm" required maxlength="100"  data-min-length="0" data-html="1" data-auto-select="true" data-animation="am-flip-x" bs-options="icon.value as icon.html for icon in getKec($viewValue)" bs-typeahead bs-on-select="oSetKec" ng-readonly="h.desa" >
                    </div>
                    <div class="col-sm-3">
                        <label title='desa'>Desa</label> : <span class="text-bold text-primary pointer" ng-click="h.desa = '';h.desa_name = ''">@{{ h.desa }}</span>
                        <input type="text" ng-model="h.desa_name" id="h_desa_name" class="form-control input-sm" required maxlength="100"  data-min-length="0" data-html="1" data-auto-select="true" data-animation="am-flip-x" bs-options="icon.value as icon.html for icon in getDesa($viewValue)" bs-typeahead bs-on-select="oSetDesa" >
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <label title='alamat'>Alamat</label>
                        <input type="text" ng-model="h.alamat" id="h_alamat" class="form-control input-sm" required  >
                    </div>
                    
                    <div class="col-sm-2">
                        <label title='rw'>RT/RW</label>
                        <input type="text" ng-model="h.rtrw" id="h_rtrw" class="form-control input-sm"  maxlength="45"  > 
                    </div>
                </div>
                <hr/>
                <h5 class="text-bold text-primary">Data Lain-Lain</h5>
                <div class="row">
                    <div class="col-sm-4">
                        <label title='kewarganegaraan'>Kewarganegaraan</label>
                        <select ng-model="h.kewarganegaraan" id="h_kewarganegaraan" class="form-control input-sm" required>
                            <option ng-repeat="v in [['WNI','WNI'], ['WNA','WNA']]" ng-value="v[0]">@{{v[1]}}</option>
                        </select>
                        
                        <label title='ayah_kandung'>Ayah Kandung</label>
                        <input type="text" ng-model="h.ayah_kandung" id="h_ayah_kandung" class="form-control input-sm"    maxlength="45"  >
                        <label title='ibu_kandung'>Ibu Kandung</label>
                        <input type="text" ng-model="h.ibu_kandung" id="h_ibu_kandung" class="form-control input-sm"    maxlength="45"  >
                        
                        <label title='status_peridmitraahan'>Status Peridmitraahan</label>
                        <select ng-model="h.status_peridmitraahan" id="h_status_peridmitraahan" class="form-control input-sm" >
                            <option ng-repeat="v in [['BELUM MEidmitraAH','Belum Meidmitraah'],['MEidmitraAH','Meidmitraah'],['DUDA','Duda'],['JANDA','Janda']]" ng-value="v[0]">@{{v[1]}}</option>
                        </select>
                        <label title='gol_darah'>Gol Darah</label>
                        <select ng-model="h.gol_darah" id="h_gol_darah" class="form-control input-sm" >
                            <option ng-repeat="v in [['A','A'],['B','B'],['AB','AB'],['O','O']]" ng-value="v[0]">@{{v[1]}}</option>
                        </select>
                        <label title='agama'>Agama</label>
                        <select ng-model="h.agama" id="h_agama" class="form-control input-sm" required>
                            <option ng-repeat="v in [['ISLAM','ISLAM']]" ng-value="v[0]">@{{v[1]}}</option>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <label title='tgl_berlaku'>Tgl Berlaku</label>
                        <input type="date" ng-model="h.tgl_berlaku" id="h_tgl_berlaku" class="form-control input-sm text-bold text-info" required   maxlength=""  >
                        <label title='pendidikan'>Pendidikan</label>
                        <select ng-model="h.pendidikan" id="h_pendidikan" class="form-control input-sm" >
                            <option ng-repeat="v in [['S2','S2'],['S1','S1'],['D3','D3'],['SMA','SMA'],['SMP','SMP'],['SD','SD'],['TIDAK SEKOLAH','TIDAK SEKOLAH']]" ng-value="v[0]">@{{v[1]}}</option>
                        </select>
                        <label title='no_telp'>No Telp</label>
                        <input type="text" ng-model="h.no_telp" id="h_no_telp" class="form-control input-sm"  maxlength="20"  >
                        <label title='no_hp'>No HP</label>
                        <input type="text" ng-model="h.no_hp" id="h_no_hp" class="form-control input-sm" required  maxlength="20"  >
                        <label title='no_wa'>No Whatsapp</label>
                        <input type="text" ng-model="h.no_wa" id="h_no_wa" class="form-control input-sm" maxlength="20"  >
                        <label title='email'>Email</label>
                        <input type="email" ng-model="h.email" id="h_email" class="form-control input-sm"    maxlength="45"  >
                    </div>
                    <div class="col-sm-4">
                        
                        <label title='foto'>Foto</label>
                        <select ng-model="h.foto" id="h_foto" class="form-control input-sm">
                            <option ng-repeat="v in m" ng-value="v.name">@{{v.name.substr(34)}}</option>
                        </select>
                        <div class="thumbnail" ng-hide="h.foto==null">
                            <img ng-src="{{\App\Sf::fileFtpAuthUrl('')}}/@{{h.foto}}" alt="Foto"  onerror="this.src='<?=url('coloradmin/assets/img/no-pict.png')?>'" width="250px">
                        </div>
                                                
                    </div>
                </div><br/>
                <div class="row">
                    <div class="col-sm-6">
                        <label title='ktp'>File KTP</label>
                        <select ng-model="h.ktp" id="h_ktp" class="form-control input-sm">
                            <option ng-repeat="v in m" ng-value="v.name">@{{v.name.substr(34)}}</option>
                        </select>
                        <div class="thumbnail" ng-hide="h.ktp==null">
                            <img ng-src="{{\App\Sf::fileFtpAuthUrl('')}}/@{{h.ktp}}" alt="KTP"  onerror="this.src='<?=url('coloradmin/assets/img/no-pict.png')?>'" width="250px">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label title='kk'>File KK</label>
                        <select ng-model="h.kk" id="h_kk" class="form-control input-sm">
                            <option ng-repeat="v in m" ng-value="v.name">@{{v.name.substr(34)}}</option>
                        </select>
                        <div class="thumbnail" ng-hide="h.kk==null">
                            <img ng-src="{{\App\Sf::fileFtpAuthUrl('')}}/@{{h.kk}}" alt="KK"  onerror="this.src='<?=url('coloradmin/assets/img/no-pict.png')?>'" width="250px">
                        </div>
                    </div>
                </div>
               
                <hr> @component('layouts.common.coloradmin.form_attr') @endcomponent
            </form>
        </div>
    </div>
</div>
<script>
app.controller('mainCtrl', ['$scope', '$http', 'NgTableParams', 'SfService', 'FileUploader', function($scope, $http, NgTableParams, SfService, FileUploader) {
    SfService.setUrl("{{url('trs_local_htr_mitra')}}");
    $scope.f = { crud: 'c', tab: 'list', trash: 0, userid: "{{Auth::user()->userid}}", plant: "{{Session::get('plant')}}" };
    $scope.h = {};
    $scope.m = [];

    $scope.tempat_lahir = [];
    <?php foreach ($kab as $k => $v){ ?>
        $scope.tempat_lahir.push({id: '<?= trim($v['name2']) ?>', label:  '<?= trim($v['name2']) ?>'});
    <?php } ?>

    var uploader = $scope.uploader = new FileUploader({
        url: "{{url('upload_file')}}",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        onBeforeUploadItem: function(item) {
            //s pattern : t : text, i : image,a : audio, v : video, p : application, x : all mime
            item.formData = [{ id: $scope.h.idmitra, path: 'trs_local_htr_mitra', s: 'i', userid: $scope.f.userid, plant: $scope.f.plant }];
        },
        onSuccessItem: function(fileItem, response, status, headers) {
            $scope.oGallery();
        }
    });

    $scope.oGallery = function() {
        SfGetMediaList('trs_local_htr_mitra/' + $scope.h.idmitra, function(jdata) {
            $scope.m = jdata.files;
            $scope.$apply();
        });
    }

    $scope.oNew = function() {
        $scope.f.tab = 'frm';
        $scope.f.crud = 'c';
        $scope.h = {};
        $scope.m = [];
        SfFormNew("#frm");

        $scope.h.jenis_kelamin = "L";
        $scope.h.tanggal_lahir = moment().toDate();
        $scope.h.tgl_berlaku = moment().toDate();
        $scope.h.kewarganegaraan = "WNI";
        $scope.h.gol_darah = "A";
        $scope.h.status_peridmitraahan = "MEidmitraAH";
        $scope.h.agama = "ISLAM";
        $scope.h.pendidikan = "S1";
        $scope.h.tempat_lahir = "TEMANGGUNG";
    }

    $scope.oCopy = function() {
        $scope.f.crud = 'c';
        $scope.h.idmitra=null;
    }

    $scope.oSearch = function(trash, order_by) {
        $scope.f.tab = "list";
        $scope.f.trash = trash;
        $scope.tableList = new NgTableParams({}, {
            getData: function($defer, params) {
                var $btn = $('button').button('loading');
                return $http.get(SfService.getUrl("_list"), {
                    params: {
                        page: $scope.tableList.page(),
                        limit: $scope.tableList.count(),
                        order_by: $scope.tableList.orderBy(),
                        q: $scope.f.q,
                        trash:$scope.f.trash,
                        plant: $scope.f.plant,
                        userid : $scope.f.userid
                    }
                }).then(function(jdata) {
                    $btn.button('reset');
                    $scope.tableList.total(jdata.data.data.total);
                    return jdata.data.data.data;
                }, function(error) {
                    $btn.button('reset');
                    swal('', error.data, 'error');
                });
            }
        });
    }

    $scope.oSave = function() {
        SfService.save("#frm", SfService.getUrl(), {
            h: $scope.h,
            f: $scope.f
        }, function(jdata) {
            $scope.oSearch();
        });
    }

    $scope.oShow = function(id) {
        SfService.show(SfService.getUrl("/" + encodeURI(id) + "/edit"), {}, function(jdata) {
            $scope.oNew();
            $scope.h = jdata.data.h;
            $scope.f.crud = 'u';
            $scope.h.tanggal_lahir = moment(jdata.data.h.tanggal_lahir).toDate();
            $scope.h.tgl_berlaku = moment(jdata.data.h.tgl_berlaku).toDate();
            $scope.h.provinsi_name = jdata.data.h.rel_mprovinces.name;
            $scope.h.kabupaten_name = jdata.data.h.rel_mregencies.name;
            $scope.h.kecamatan_name = jdata.data.h.rel_mdistricts.name;
            $scope.h.desa_name = jdata.data.h.rel_mvillages.name;
            $scope.oGallery();
            
        });
    }

    $scope.oDel = function(id,isRestore) {
        if (id == undefined) {
            var id = $scope.h.idmitra;
        }
        SfService.delete(SfService.getUrl("/" + encodeURI(id)), {restore:isRestore}, function(jdata) {
            $scope.oSearch();
        });
    }

    $scope.oRestore = function(id) {
       $scope.oDel(id,1);
    }

    $scope.oLookup = function(id, selector, obj) {
        switch (id) {
            /*case 'parent':
                SfLookup(SfService.getUrl("_lookup"), function(id, name, jsondata) {
                    $("#" + selector).val(id).trigger('input');;
                });
                break;*/
            default:
                swal('Sorry', 'Under construction', 'error');
                break;
        }
    }

    $scope.getProvinsi = function(viewValue) {
        return SfService.typeahead("{{url('trs_local_htr_mitra_get_provinsi')}}", {
            q: viewValue,
            limit: 10,
            
        });
    }

    $scope.oSetProvinsi = function(){
        SfService.httpGet("{{url('trs_local_htr_mitra_set_provinsi')}}", { id: $scope.h.provinsi_name}, function(jdata) {
           $scope.h.provinsi_name = jdata.data.data.name;
           $scope.h.provinsi = jdata.data.data.id;

        });
    }

    $scope.getKab = function(viewValue) {
        return SfService.typeahead("{{url('trs_local_htr_mitra_get_kab')}}", {
            q: viewValue,
            prov_id: $scope.h.provinsi,
            limit: 10,
            
        });
    }

    $scope.oSetKab = function(){
        SfService.httpGet("{{url('trs_local_htr_mitra_set_kab')}}", { id: $scope.h.kabupaten_name}, function(jdata) {
           $scope.h.kabupaten_name = jdata.data.data.name;
           $scope.h.kabupaten = jdata.data.data.id;

        });
    }

     $scope.getKec = function(viewValue) {
        return SfService.typeahead("{{url('trs_local_htr_mitra_get_kec')}}", {
            q: viewValue,
            reg_id: $scope.h.kabupaten,
            limit: 10,
            
        });
    }

    $scope.oSetKec = function(){
        SfService.httpGet("{{url('trs_local_htr_mitra_set_kec')}}", { id: $scope.h.kecamatan_name}, function(jdata) {
           $scope.h.kecamatan_name = jdata.data.data.name;
           $scope.h.kecamatan = jdata.data.data.id;

        });
    }

     $scope.getDesa = function(viewValue) {
        return SfService.typeahead("{{url('trs_local_htr_mitra_get_desa')}}", {
            q: viewValue,
            dis_id: $scope.h.kecamatan,
            limit: 10,
            
        });
    }

    $scope.oSetDesa = function(){
        SfService.httpGet("{{url('trs_local_htr_mitra_set_desa')}}", { id: $scope.h.desa_name}, function(jdata) {
           $scope.h.desa_name = jdata.data.data.name;
           $scope.h.desa = jdata.data.data.id;

        });
    }

    $scope.oLog=function(){
        SfLog('trs_local_htr_mitra',$scope.h.idmitra);
    }

    $scope.goToPendaftaran = function(idmitra){
        window.location.href = "{{ url('trs_local_ctr_pendaftaran') }}?redirect=1&idmitra=" + idmitra;
    }

    $scope.oSearch();
}]);
</script>
@endsection