<link href="{{url('coloradmin')}}/assets/plugins/fullcalendar/fullcalendar.print.css" rel="stylesheet" media='print' />
<link href="{{url('coloradmin')}}/assets/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" />
<script src="{{url('coloradmin')}}/assets/plugins/fullcalendar/lib/moment.min.js"></script>
<script src="{{url('coloradmin')}}/assets/plugins/fullcalendar/fullcalendar.min.js"></script>
<div>{{$slot}}</div>
<div id="{{$id}}" class="vertical-box-column p-15 calendar"></div>

<script>

var handleCalendarDemo = function() {

    $('#external-events .fc-event').each(function() {

        $(this).data('event', {
            title: $.trim($(this).text()), // use the element's text as the event title
            stick: true, // maintain when user navigates (see docs on the renderEvent method)
            color: ($(this).attr('data-color')) ? $(this).attr('data-color') : ''
        });
        $(this).draggable({
            zIndex: 999,
            revert: true, // will cause the event to go back to its
            revertDuration: 0 //  original position after the drag
        });
    });

    var date = new Date();
    var currentYear = date.getFullYear();
    var currentMonth = date.getMonth() + 1;
    currentMonth = (currentMonth < 10) ? '0' + currentMonth : currentMonth;

    $('#{{$id}}').fullCalendar({
        header: {
            left: 'month,agendaWeek,agendaDay',
            center: 'title',
            right: 'prev,today,next '
        },
        height:1200,
        droppable: false, // this allows things to be dropped onto the calendar
        selectable: true,
        selectHelper: true,
        editable: false,
        eventLimit: true, // allow "more" link when too many events
        events: mainCtrl().getListCalendar(),
        eventClick:function(info){

        }
    });
};

    var Calendar = function() {
    "use strict";
    return {
        //main function
        init: function() {
            handleCalendarDemo();
        }
    };
}();
handleCalendarDemo();
    </script>