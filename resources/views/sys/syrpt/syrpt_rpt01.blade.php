@extends('layouts.coloradmin')
<!-- ------------------------------------------------------------------------------- -->
@section('title')Static Report @stop
<!-- ------------------------------------------------------------------------------- -->
@section('title-small') @stop
<!-- ------------------------------------------------------------------------------- -->
@section('breadcrumb')
<span>Display</span> @stop
<!-- ------------------------------------------------------------------------------- -->
@section('content')
<div class="row">
    <div class="col-sm-3">
        <div class="panel panel-success">
            <div class="panel-heading">
                @component('layouts.common.coloradmin.panel_button') @endcomponent Report
            </div>
            <div class="panel-body">
            	<div class="input-group">
                        <input type="text" class="form-control input-sm" ng-model="f.q" ng-enter="oSearch()" placeholder="Search">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-success btn-sm" ng-click="oSearch()"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                <div id="div1" class="table-responsive">
                    <table ng-table="tableList" show-filter="false" class="table table-condensed table-hover" style="white-space: nowrap;">
                        <tr ng-repeat="v in $data" class="pointer" ng-click="oRpt(v.rptid)">
                            <td title="'Report Name'" filter="{rptname: 'text'}" sortable="'rptname'">@{{v.rptname}}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-9">
        <div class="panel panel-success">
            <div class="panel-heading">
                @component('layouts.common.coloradmin.panel_button') @endcomponent Result
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-condensed table-bordered table-striped" style="white-space: nowrap;">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th ng-repeat="(k,v) in simulatelist[0]">@{{k}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="(key,value) in simulatelist">
                                <td class="text-right">@{{key+1}}.</td>
                                <td ng-repeat="(k,v) in value">@{{v}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
app.controller('mainCtrl', ['$scope', '$http', 'NgTableParams', 'SfService', 'FileUploader', function($scope, $http, NgTableParams, SfService, FileUploader) {
    SfService.setUrl("{{url('sys_syrpt')}}");

    $scope.f = {
        crud: 'c',
        tab: 'list',
        trash: 0,
        userid: "{{Auth::user()->userid}}",
        plant: "{{Session::get('plant')}}"
    };

    $scope.oSearch = function(trash, order_by) {
        $scope.f.tab = "list";
        $scope.f.trash = trash;
        $scope.tableList = new NgTableParams({}, {
            getData: function($defer, params) {
                var $btn = $('button').button('loading');
                return $http.get(SfService.getUrl("_list"), {
                    params: {
                        page: $scope.tableList.page(),
                        limit: $scope.tableList.count(),
                        order_by: $scope.tableList.orderBy(),
                        q: $scope.f.q,
                        trash: $scope.f.trash
                    }
                }).then(function(jdata) {
                    $btn.button('reset');
                    $scope.tableList.total(jdata.data.data.total);
                    return jdata.data.data.data;
                }, function(error) {
                    $btn.button('reset');
                    swal('', error.data, 'error');
                });
            }
        });
    }

    $scope.oRpt = function(id) {
        SfService.httpGet(SfService.getUrl('_run_table'), { id: id }, function(jdata) {
            console.log(jdata.data);
            $scope.simulatelist = jdata.data.data;
        });
    }

    $scope.oSearch();
}]);
</script>
@stop