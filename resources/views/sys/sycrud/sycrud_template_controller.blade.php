<?="<?php"?>

namespace App\Http\Controllers\{{$conf['namespace']}};

use App\Http\Controllers\Controller;
use App\Model\{{$conf['namespace']}}\{{$conf['tableCamel']}};
use Auth;
use DB;
use App\Sf;
use Illuminate\Http\Request;

class {{$conf['tableCamel']}}Controller extends Controller {

	public function index(Request $request) {
		if (!$plant = Sf::isPlant()) {
			return Sf::selectPlant();
		}

		Sf::log("{{$conf['route']}}", "{{$conf['tableCamel']}}Controller@" . __FUNCTION__, "Open Page  ", "link");

		return view('{{str_replace('\\','.',$conf['pathLower'])}}{{$conf['tableLower']}}.{{$conf['tableLower']}}_frm', compact(['request','plant']));
	}

	public function getList(Request $request) {
		if (!Sf::allowed('{{strtoupper($conf['route'])}}_R')) {
			return response()->json(Sf::reason(), 401);
		}
		$data = {{$conf['tableCamel']}}::where(function ($q) use ($request) {
<?php foreach ($s as $k => $v): ?>
<?php if (in_array($v->DATA_TYPE, ['varchar', 'char', 'text', 'int']) && !in_array($v->COLUMN_NAME, ['created_by', 'updated_by', 'text', 'int'])): ?>
			$q->orWhere('{{$v->COLUMN_NAME}}', 'like', "%" . @$request->q . "%");
<?php endif?>
<?php endforeach?>
		})
			//->where('plant',$request->plant)
			->orderBy(isset($request->order_by) ? substr($request->order_by, 1) : '{{$h->pk}}', substr(@$request->order_by, 0, 1) == '-' ? 'desc' : 'asc');
			if ($request->trash == 1) {
				$data = $data->onlyTrashed();
			}
			$data = $data->paginate(isset($request->limit) ? $request->limit : 10);
		return response()->json(compact(['data']));
	}

	public function getLookup(Request $request) {
		$data = {{$conf['tableCamel']}}::where(function ($q) use ($request) {
<?php foreach ($s as $k => $v): ?>
<?php if (in_array($v->DATA_TYPE, ['varchar', 'char', 'text', 'int']) && !in_array($v->COLUMN_NAME, ['created_by', 'updated_by', 'text', 'int'])): ?>
			$q->orWhere('{{$v->COLUMN_NAME}}', 'like', "%" . @$request->q . "%");
<?php endif?>
<?php endforeach?>
		})
			//->where('plant',$request->plant)
			->orderBy(isset($request->order_by) ? substr($request->order_by, 1) : '{{$h->pk}}', substr(@$request->order_by, 0, 1) == '-' ? 'desc' : 'asc');
			$data = $data->paginate(isset($request->limit) ? $request->limit : 10);
		return view('sys.system.dialog.sflookup', compact(['data', 'request']));
	}

	public function store(Request $request) {
		$req = json_decode(request()->getContent());
		$h = $req->h;
		$f = $req->f;

		try {
			$arr = array_merge((array) $h, ['plant'=>$f->plant,'updated_at' => date('Y-m-d H:i:s')]);
			if ($f->crud == 'c') {
				if (!Sf::allowed('{{strtoupper($conf['route'])}}_C')) {
					return response()->json(Sf::reason(), 401);
				}
				$data = new {{$conf['tableCamel']}}();
				$arr = array_merge($arr, ['created_by' => Auth::user()->userid, 'created_at' => date('Y-m-d H:i:s')]);
				$data->create($arr);
<?php if ($h->ai == 1): ?>
				$id = DB::getPdo()->lastInsertId();
<?php else: ?>
				$id=$h->{{$h->pk}};
<?php endif?>
				Sf::log("{{$conf['route']}}", $id, "Create {{$h->title}} ({{$conf['tableLower']}}) {{$h->pk}} : " . $id, "create");
				return 'created';
			} else {
				if (!Sf::allowed('{{strtoupper($conf['route'])}}_U')) {
					return response()->json(Sf::reason(), 401);
				}
				$data = {{$conf['tableCamel']}}::find($h->{{$h->pk}});
				$data->update($arr);
				$id = $data->{{$h->pk}};
				Sf::log("{{$conf['route']}}", $id, "Update {{$h->title}} ({{$conf['tableLower']}}) {{$h->pk}} : " . $id, "update");
				return 'updated';
			}

		} catch (\Exception $e) {
			return response()->json($e->getMessage(), 500);
		}
	}

	public function edit($id) {
		$h = {{$conf['tableCamel']}}::where('{{$h->pk}}', $id)->withTrashed()->first();
		return response()->json(compact(['h']));
	}

	public function destroy($id,Request $request) {
		try {
			$data = {{$conf['tableCamel']}}::where('{{$h->pk}}', $id)->withTrashed()->first();
			if ($request->restore == 1) {
				if (!Sf::allowed('{{strtoupper($conf['route'])}}_S')) {
					return response()->json(Sf::reason(), 401);
				}
				$data->restore();
				Sf::log("{{$conf['route']}}", $id, "Restore {{$h->title}} ({{$conf['tableLower']}}) {{$h->pk}} : " . $id, "restore");
				return 'restored';
			} else {
				if (!Sf::allowed('{{strtoupper($conf['route'])}}_D')) {
					return response()->json(Sf::reason(), 401);
				}
				$data->delete();
				Sf::log("{{$conf['route']}}", $id, "Delete {{$h->title}} ({{$conf['tableLower']}}) {{$h->pk}} : " . $id, "delete");
				return 'deleted';
			}
		} catch (\Exception $e) {
			return response()->json($e->getMessage(), 500);
		}
	}
}