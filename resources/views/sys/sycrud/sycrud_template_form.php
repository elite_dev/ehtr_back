@extends('layouts.coloradmin')
<!-- ------------------------------------------------------------------------------- -->
@section('title')<?=$h->title?> @stop
<!-- ------------------------------------------------------------------------------- -->
@section('title-small') @stop
<!-- ------------------------------------------------------------------------------- -->
@section('breadcrumb')
<span ng-show="f.tab=='list'">Data List</span>
<span ng-show="f.tab=='frm'">Form Entry</span> @stop
<!-- ------------------------------------------------------------------------------- -->
@section('content')
<div class="panel panel-success">
    <div class="panel-heading">
        @component('layouts.common.coloradmin.panel_button') @endcomponent @yield('breadcrumb')
    </div>
    <div class="panel-body">
        <div class="m-b-5 form-inline">
            <div class="pull-right">
                <div ng-show="f.tab=='list'">
                    @component('layouts.common.coloradmin.guide',['tag'=>'<?=$conf['route']?>']) @endcomponent
                    <div class="input-group">
                        <div class="btn-group">
                            <button type="button" class="btn btn-success btn-sm" onclick="SfExportExcel('div1')"><i class="fa fa fa-file-excel-o"></i></button>
                            <button type="button" class="btn btn-success btn-sm" ng-click="oPrint()"><i class="fa fa fa-print"></i></button>
                            <button type="button" class="btn btn-success btn-sm"  ng-click="oSearch(1)"><i class="fa fa fa-recycle"></i></button>
                        </div>
                    </div>
                    <div class="input-group">
                        <input type="text" class="form-control input-sm" ng-model="f.q" ng-enter="oSearch()" placeholder="Search">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-success btn-sm" ng-click="oSearch()"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
                <div ng-show="f.tab=='frm'">
                    <button type="button" class="btn btn-sm btn-success" ng-click="oSave()" ng-show="f.crud=='c' && f.trash!=1"><i class="fa fa-save"></i> Create</button>
                    <button type="button" class="btn btn-sm btn-success" ng-click="oSave()" ng-show="f.crud=='u' && f.trash!=1"><i class="fa fa-save"></i> Update</button>
                    <button type="button" class="btn btn-sm btn-warning" ng-click="oCopy()" ng-show="f.crud=='u'"><i class="fa fa-copy"></i> Copy</button>
                    <button type="button" class="btn btn-sm btn-danger" ng-click="oDel()" ng-show="f.crud=='u'&& f.trash!=1"><i class="fa fa-trash"></i> Delete</button>
                    <button type="button" class="btn btn-sm btn-warning" ng-click="oRestore()" ng-show="f.crud=='u' && f.trash==1"><i class="fa fa-recycle"></i> Restore</button>
                    <button type="button" class="btn btn-sm btn-info" ng-click="oLog()" ng-show="f.crud=='u'"><i class="fa fa-clock-o"></i> Log</button>
<?php if ($h->isfile == 1): ?>
                    @component('layouts.common.coloradmin.upload') @endcomponent
<?php endif?>
                    <span ng-if="f.crud!='c'"> @component('layouts.common.coloradmin.chat',['route'=>'<?=$conf['route']?>','id'=>'h.<?=$h->pk?>'])  @endcomponent </span>
                </div>
            </div>
            <button type="button" class="btn btn-sm btn-inverse" ng-click="oNew()" ng-attr-title="Buat Baru" ng-show="f.tab=='list' && f.trash!=1"><i class="fa fa-plus"></i> New</button>
            <button type="button" class="btn btn-sm btn-inverse" ng-click="f.tab='list'" ng-attr-title="Kembali ke Halaman Awal" ng-show="f.tab=='frm'"><i class="fa fa-arrow-left"></i> Back</button>
        </div>
        <br>
        <div ng-show="f.tab=='list'">
            <div class="alert alert-warning" ng-show="f.trash==1"><i class="fa fa-warning fa-2x"></i> This is deleted item<br>Trashed</div>
            <div id="div1" class="table-responsive">
                <table ng-table="tableList" show-filter="false" class="table table-condensed table-hover" style="white-space: nowrap;">
                    <tr ng-repeat="v in $data" class="pointer" ng-click="oShow(v.<?=$h->pk?>)">
<?php foreach ($s as $k => $v): ?>
<?php if (!in_array($v->COLUMN_NAME, ['created_by', 'updated_by', 'updated_at', 'created_at', 'deleted_at', 'trash', 'isaktif', 'text', 'int'])): ?>
                        <td title="'<?=$v->CAPTION?>'" filter="{<?=$v->COLUMN_NAME?>: 'text'}" sortable="'<?=$v->COLUMN_NAME?>'">@{{v.<?=$v->COLUMN_NAME?>}}</td>
<?php endif?>
<?php endforeach?>
                    </tr>
                </table>
            </div>
        </div>
        <div ng-show="f.tab=='frm'">
            <form action="#" name="frm" id="frm">
                <div class="row">
<?php
$count_element = 0;
foreach ($s as $k => $v) {
	if ($v->ELEMENT != 'none') {
		$count_element++;
	}
}
$chunkrows = ceil($count_element / 3);
$chunk = array_chunk($s, $chunkrows);
?>
<?php foreach ($chunk as $key => $value): ?>
                    <div class="col-sm-4">
<?php foreach ($value as $k => $v): ?>
<?php if ($v->COLUMN_NAME == $h->pk):
	$pkattr = " ng-readonly=\"f.crud!='c' " . ($h->ai == 1 ? '|| true' : '') . " \" " . ($h->ai == 1 ? 'placeholder="auto"' : '') . "";
else:
	$pkattr = "";
endif
?>
<?php if (in_array($v->ELEMENT, ['select'])): ?>
                        <label title='<?=$v->COLUMN_NAME?>'><?=$v->CAPTION?></label>
                        <select ng-model="h.<?=$v->COLUMN_NAME?>" id="h_<?=$v->COLUMN_NAME?>" class="form-control input-sm" <?=$v->REQUIRED?>>
                            <option ng-repeat="v in [['1','Option 1'],['2','Option 2']]" ng-value="v[0]">@{{v[1]}}</option>
                        </select>
<?php elseif (in_array($v->ELEMENT, ['select-plus'])): ?>
                        <label title='<?=$v->COLUMN_NAME?>'><?=$v->CAPTION?></label>
                        <div class="input-group">
                            <select ng-model="h.<?=$v->COLUMN_NAME?>" id="h_<?=$v->COLUMN_NAME?>" class="form-control input-sm" <?=$v->REQUIRED?>>
                                <option ng-repeat="v in [['1','Option 1'],['2','Option 2']]" ng-value="v[0]">@{{v[1]}}</option>
                            </select>
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-sm btn-default" ng-click="addCombo('<?=$v->COLUMN_NAME?>')"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
<?php elseif (in_array($v->ELEMENT, ['textarea'])): ?>
                        <label title='<?=$v->COLUMN_NAME?>'><?=$v->CAPTION?></label>
                        <textarea ng-model="h.<?=$v->COLUMN_NAME?>" id="h_<?=$v->COLUMN_NAME?>" class="form-control input-sm" <?=$v->REQUIRED?>  <?=$pkattr?>></textarea>
<?php elseif (in_array($v->ELEMENT, ['lookup'])): ?>
                        <label title='<?=$v->COLUMN_NAME?>'><?=$v->CAPTION?></label>
                        <div class="input-group">
                            <input type="text" ng-model="h.<?=$v->COLUMN_NAME?>" id="h_<?=$v->COLUMN_NAME?>" class="form-control input-sm" <?=$v->REQUIRED?> readonly maxlength="<?=$v->CHARACTER_MAXIMUM_LENGTH?>"  <?=$pkattr?>>
                            <div class="input-group-btn">
                                <button class="btn btn-default btn-sm" type="button" ng-click="oLookup('<?=$v->COLUMN_NAME?>','h_<?=$v->COLUMN_NAME?>')"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
<?php elseif (in_array($v->ELEMENT, ['none'])): ?>
<?php else: ?>
                        <label title='<?=$v->COLUMN_NAME?>'><?=$v->CAPTION?></label>
                        <input type="<?=@explode(' ', $v->ELEMENT)[0]?>" ng-model="h.<?=$v->COLUMN_NAME?>" id="h_<?=$v->COLUMN_NAME?>" class="form-control input-sm" <?=$v->REQUIRED?> <?=$v->ELEMENT == 'text readonly' ? 'readonly' : ''?> <?=$v->ELEMENT == 'text number' ? 'ng-pattern="/[0-9.,]+/" format="number"' : ''?> maxlength="<?=$v->CHARACTER_MAXIMUM_LENGTH?>"  <?=$pkattr?>>
<?php endif?>
<?php endforeach?>
                    </div>
<?php endforeach?>
                </div>
                <hr> @component('layouts.common.coloradmin.form_attr') @endcomponent
            </form>
        </div>
    </div>
</div>
<?="<script>"?>

app.controller('mainCtrl', ['$scope', '$http', 'NgTableParams', 'SfService', 'FileUploader', function($scope, $http, NgTableParams, SfService, FileUploader) {
    SfService.setUrl("{{url('<?=$conf['route']?>')}}");
    $scope.f = { crud: 'c', tab: 'list', trash: 0, userid: "{{Auth::user()->userid}}", plant: "{{Session::get('plant')}}" };
    $scope.h = {};
    $scope.m = [];

     var uploader = $scope.uploader = new FileUploader({
        url: "{{url('upload_file')}}",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        onBeforeUploadItem: function(item) {
            //s pattern : t : text, i : image,a : audio, v : video, p : application, x : all mime
            item.formData = [{ id: $scope.h.<?=$h->pk?>, path: '<?=$conf['route']?>', s: 'i', userid: $scope.f.userid, plant: $scope.f.plant }];
        },
        onSuccessItem: function(fileItem, response, status, headers) {
            $scope.oGallery();
        }
    });

    $scope.oGallery = function() {
        SfGetMediaList('<?=$conf['route']?>/' + $scope.h.<?=$h->pk?>, function(jdata) {
            $scope.m = jdata.files;
            $scope.$apply();
        });
    }

    $scope.oNew = function() {
        $scope.f.tab = 'frm';
        $scope.f.crud = 'c';
        $scope.h = {};
        $scope.m = [];
        SfFormNew("#frm");
    }

    $scope.oCopy = function() {
        $scope.f.crud = 'c';
        $scope.h.<?=$h->pk?>=null;
    }

    $scope.oSearch = function(trash, order_by) {
        $scope.f.tab = "list";
        $scope.f.trash = trash;
        $scope.tableList = new NgTableParams({}, {
            getData: function($defer, params) {
                var $btn = $('button').button('loading');
                return $http.get(SfService.getUrl("_list"), {
                    params: {
                        page: $scope.tableList.page(),
                        limit: $scope.tableList.count(),
                        order_by: $scope.tableList.orderBy(),
                        q: $scope.f.q,
                        trash:$scope.f.trash,
                        plant: $scope.f.plant,
                        userid : $scope.f.userid
                    }
                }).then(function(jdata) {
                    $btn.button('reset');
                    $scope.tableList.total(jdata.data.data.total);
                    return jdata.data.data.data;
                }, function(error) {
                    $btn.button('reset');
                    swal('', error.data, 'error');
                });
            }
        });
    }

    $scope.oSave = function() {
        SfService.save("#frm", SfService.getUrl(), {
            h: $scope.h,
            f: $scope.f
        }, function(jdata) {
            $scope.oSearch();
        });
    }

    $scope.oShow = function(id) {
        SfService.show(SfService.getUrl("/" + encodeURI(id) + "/edit"), {}, function(jdata) {
            $scope.oNew();
            $scope.h = jdata.data.h;
            $scope.f.crud = 'u';
            $scope.oGallery();
            if(chatCtrl() != undefined){
                chatCtrl().listChat();
            }
        });
    }

    $scope.oDel = function(id,isRestore) {
        if (id == undefined) {
            var id = $scope.h.<?=$h->pk?>;
        }
        SfService.delete(SfService.getUrl("/" + encodeURI(id)), {restore:isRestore}, function(jdata) {
            $scope.oSearch();
        });
    }

    $scope.oRestore = function(id) {
       $scope.oDel(id,1);
    }

    $scope.oLookup = function(id, selector, obj) {
        switch (id) {
            /*case 'parent':
                SfLookup(SfService.getUrl("_lookup"), function(id, name, jsondata) {
                    $("#" + selector).val(id).trigger('input');;
                });
                break;*/
            default:
                swal('Sorry', 'Under construction', 'error');
                break;
        }
    }

     $scope.oLog=function(){
        SfLog('<?=$conf['route']?>',$scope.h.<?=$h->pk?>);
    }

    $scope.oSearch();
}]);
</script>
@endsection