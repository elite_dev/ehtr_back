<?="<?php"?>

Route::group(['namespace' => '{{$conf['namespace']}}', 'middleware' => ['web', 'auth']], function () {
	Route::resource('/{{$conf['route']}}', '{{$conf['tableCamel']}}Controller');
	Route::get('/{{$conf['route']}}_list', '{{$conf['tableCamel']}}Controller@getList');
	Route::get('/{{$conf['route']}}_lookup', '{{$conf['tableCamel']}}Controller@getLookup');
});