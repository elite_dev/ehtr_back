<?="<?php"?>

namespace App\Model\{{$conf['namespace']}};

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class {{$conf['tableCamel']}} extends Model {

	use SoftDeletes;

	protected $connection = '{{isset($h->conn)?$h->conn:'mysql'}}';
	public $incrementing = {{$h->ai==1?'true':'false'}};
	public $timestamps = {{$h->timestamps==1?'true':'false'}};
	protected $hidden = [];
	protected $dates = ['deleted_at'];
	protected $table = '{{$conf['tableLower']}}';
	protected $primaryKey = "{{$h->pk}}";
	protected $fillable = [
<?php foreach ($s as $key => $v): ?>
		'{{$v->COLUMN_NAME}}',
<?php endforeach?>
	];

	public function rel_created_by() {
		return $this->belongsTo('App\Model\Sys\Syuser', 'created_by');
	}

}
