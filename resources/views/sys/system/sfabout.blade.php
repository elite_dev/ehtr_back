@extends('layouts.coloradmin')
@section('title')Tentang @stop
@section('title-small') @stop
@section('breadcrumb')
@stop
@section('content')
<?php
$sf_version = "1.0.0";
$arr = [
	[
		"subj" => "Theme & Color",
		"desc" => "Template skin, css packaged",
		"platform" => "Color Admin",
		"version" => "2.0",
	], [
		"subj" => "Front End Framework",
		"desc" => "Javascript logical programming front end",
		"platform" => "Angular",
		"version" => "1.7",
	], [
		"subj" => "Back End Framework",
		"desc" => "PHP logical programming back end / server side",
		"platform" => "Laravel",
		"version" => app()::VERSION,
	], [
		"subj" => "Application Framework",
		"desc" => "Combine many periperal in harmony",
		"platform" => "Savetime Framework (SF)",
		"version" => $sf_version,
	], /* [
		"subj" => "",
		"desc" => "",
		"platform" => "",
		"version" => "",
	]*/
];
?>
<!-- begin invoice -->
<div class="invoice">
    <div class="invoice-company">
        <span class="pull-right hidden-print">
                    <a href="javascript:;" onclick="window.print()" class="btn btn-sm btn-success m-b-10"><i class="fa fa-print m-r-5"></i> Print</a>
                    </span>
        {{\App\Sf::getParsys('APP_LABEL')}}
    </div>
    <div class="invoice-header">
        <div class="invoice-from">
            <small>Development Team</small>
            <address class="m-t-5 m-b-5">
                <strong>MSD Temanggung.</strong><br />
                Villa Asri Madureso A1<br />
                Temanggung<br />
                Phone: (0293) 4962891<br />
                WA: <a href="https://api.whatsapp.com/send?phone=6282138950006&text=Halo%20Mas,%20saya%20mau%20aplikasinya" target="_blank">082138950006</a><br />
                Mail: <a href="mailto:g.ztroo@gmail.com">g.ztroo@gmail.com</a>
            </address>
        </div>
        
        <div class="invoice-date">
            <small>Release Date</small>
            <div class="date m-t-5">February 29,2020</div>
            <div class="invoice-detail">
                Version<br />
                v.{{$sf_version}}
            </div>
        </div>
    </div>
    <div class="invoice-content">
        <div class="table-responsive">
            <table class="table table-invoice">
                <thead>
                    <tr>
                        <th>TOOLS DESCRIPTION</th>
                        <th>PLATFORM</th>
                        <th>VERSION</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($arr as $k => $v): ?>
                    <tr>
                        <td>
                            {{$v['subj']}}<br />
                            <small>{{$v['desc']}}</small>
                        </td>
                        <td>{{$v['platform']}}</td>
                        <td>{{$v['version']}}</td>
                    </tr>
                    <?php endforeach?>
                </tbody>
            </table>
        </div>
        <div class="invoice-price">
            <div class="invoice-price-left">
                <div class="invoice-price-row">
                    <div class="sub-price">
                        <small>BACKEND</small>
                        Laravel {{app()::VERSION}}
                    </div>
                    <div class="sub-price">
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="sub-price">
                        <small>FRONTEND</small>
                        Angular 1.*
                    </div>
                </div>
            </div>
            <div class="invoice-price-right" style="text-align: center;">
                <small>MODULE</small> {{\App\Sf::getParsys('APP_LABEL')}}
            </div>
        </div>
    </div>
    <div><hr>
        <h3>Release Note!</h3>
        <b>Version 1.0.0 (29th Feb 2020) </b>
        <h3>FRAMEWORK</h3>
        <b>Version 3.1.1 (8th July 2019)</b>
        <p>
            <ul>
                <li>Upgrade laravel version from 5.6 to 5.8</li>
                <li>Add Barcode Fiture</li>
                <li>Add Email Rule</li>
                <li>Add PDF Creator (DOM)</li>
                <li>Add Autocomplete Textbox</li>
                <li>Add Awnum Input Number</li>
                <li>Add Chatting in form</li>
            </ul>
        </p>
        <b>Version 3.0.1 (2nd February 2018) </b>
        <p>
            <ul>
                <li>Form Calendar</li>
                <li>Add Fusion Chart</li>
                <li>Add File Attachment</li>
            </ul>
        </p>
        <b>Version 3.0.0 (17th December 2018) </b>
        <p>
            <ul>
                <li>Core SF Framework</li>
            </ul>
        </p>
        <b>Version 2.0.0 (10th November 2018) </b>
        <p>
            <ul>
                <li>SF Framework 2</li>
            </ul>
        </p>
        <b>Version 1.0.0 (26th November 2016) </b>
        <p>
            <ul>
                <li>SF Framework 1</li>
            </ul>
        </p>
    </div>
    
    <div class="invoice-footer text-muted">
        <p class="text-center m-b-5">
            THANK YOU FOR YOUR BUSINESS
        </p>
        <p class="text-center">
            <span class="m-r-10"><i class="fa fa-globe"></i> msd-temanggung.co.id</span>
            <!-- <span class="m-r-10"><i class="fa fa-phone"></i> T:016-18192302</span> -->
            <span class="m-r-10"><i class="fa fa-envelope"></i> g.ztroo@gmail.com</span>
        </p>
    </div>
</div>
<!-- end invoice -->
@endsection