@extends('layouts.coloradmin')
@section('title')Dashboard @stop
@section('title-small')Sample @stop
@section('breadcrumb')
<span>Backend</span>
@stop
@section('content')
<script src="{{url('coloradmin')}}/assets/plugins/flot/jquery.flot.min.js"></script>
<script src="{{url('coloradmin')}}/assets/plugins/flot/jquery.flot.time.min.js"></script>
<script src="{{url('coloradmin')}}/assets/plugins/flot/jquery.flot.resize.min.js"></script>
<script src="{{url('coloradmin')}}/assets/plugins/flot/jquery.flot.pie.min.js"></script>
<script src="{{url('coloradmin')}}/assets/plugins/sparkline/jquery.sparkline.js"></script>
<div class="">
    <div class="row">
        <!-- begin col-3 -->
        <div class="col-md-3 col-sm-6">
            <div class="widget widget-stats bg-green">
                <div class="stats-icon stats-icon-lg"><i class="fa fa-user fa-fw"></i></div>
                <div class="stats-title">USER ID</div>
                <div class="stats-number">{{Auth::user()->userid}}</div>
                <div class="stats-progress progress">
                    <div class="progress-bar" style="width: 70.1%;"></div>
                </div>
                <div class="stats-desc">{{Auth::user()->username}}</div>
            </div>
        </div>
        <!-- end col-3 -->
        <!-- begin col-3 -->
        <div class="col-md-3 col-sm-6">
            <div class="widget widget-stats bg-blue">
                <div class="stats-icon stats-icon-lg"><i class="fa fa-inbox fa-fw"></i></div>
                <div class="stats-title">MESSAGE INBOX</div>
                <div class="stats-number">0</div>
                <div class="stats-progress progress">
                    <div class="progress-bar" style="width: 40.5%;"></div>
                </div>
                <div class="stats-desc">More Message</div>
            </div>
        </div>
        <!-- end col-3 -->
        <!-- begin col-3 -->
        <div class="col-md-3 col-sm-6">
            <div class="widget widget-stats bg-purple">
                <div class="stats-icon stats-icon-lg"><i class="fa fa-shopping-cart fa-fw"></i></div>
                <div class="stats-title">APP NAME</div>
                <div class="stats-number">{{\App\Sf::getParsys('APP_LABEL')}}</div>
                <div class="stats-progress progress">
                    <div class="progress-bar" style="width: 76.3%;"></div>
                </div>
                <div class="stats-desc">Elite Develoment</div>
            </div>
        </div>
        <!-- end col-3 -->
        <!-- begin col-3 -->
        <div class="col-md-3 col-sm-6">
            <div class="widget widget-stats bg-black">
                <div class="stats-icon stats-icon-lg"><i class="fa fa-trophy fa-fw"></i></div>
                <div class="stats-title">STATUS</div>
                <div class="stats-number"><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                <div class="stats-progress progress">
                    <div class="progress-bar" style="width: 54.9%;"></div>
                </div>
                <div class="stats-desc">Logged in</div>
            </div>
        </div>
        <!-- end col-3 -->
    </div>
    
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-inverse" data-sortable-id="index-1">
                <div class="panel-heading">
                    @component('layouts.common.coloradmin.panel_button') @endcomponent
                    <h4 class="panel-title">Session</h4>
                </div>
                <div class="panel-body">
                    {{Auth::user()->username}}
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">Application</div>
                <div class="panel-body">
                    {{\App\Sf::getParsys('APP_DESC')}}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-success">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif
                    You are logged in!
                </div>
            </div>
            <div class="panel panel-warning">
                <div class="panel-heading">Current Time</div>
                <div class="panel-body">
                    {{date('d F Y H:i')}}
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{url('coloradmin')}}/assets/plugins/bootstrap-calendar/js/bootstrap_calendar.js"></script>
<script>
var monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",  "Juli", "Augustus", "September", "Oktober", "November", "Desember"];
var dayNames = ["Min","Sen", "Sel", "Rab", "Kam", "Jum", "Sab"];

app.controller('mainCtrl', ['$scope', '$http', 'NgTableParams', 'SfService', function($scope, $http, NgTableParams, SfService) {
    SfService.setUrl("{{url('trs_local_ctr_dashboard')}}");
var now = new Date(),
    thismonth = now.getMonth() + 1,
    thisyear = now.getFullYear();

    $scope.handleScheduleCalendar = function() {    
        var now = new Date(),
            month = now.getMonth() + 1,
            year = now.getFullYear();
          
        var calendarTarget = $('#schedule-calendar');
        $(calendarTarget).calendar({
            months: monthNames,
            days: dayNames,
            req_ajax: {
                type: 'get',
                url: SfService.getUrl("_calendar")
            },
            //events: events,                       
            popover_options:{
                placement: 'top',
                html: true
            }
        });
        
        $(calendarTarget).find('td.event').each(function() {
            var backgroundColor = $(this).css('background-color');
            $(this).removeAttr('style');
            $(this).find('a').css('background-color', backgroundColor);
        });
        $(calendarTarget).find('.icon-arrow-left, .icon-arrow-right').parent().on('click', function() {
            $(calendarTarget).find('td.event').each(function() {
                var backgroundColor = $(this).css('background-color');
                $(this).removeAttr('style');
                $(this).find('a').css('background-color', backgroundColor);
            });
        });

        $(".visualmonthyear").bind("DOMSubtreeModified",function(){  
        var month = $(".visualmonthyear").attr('data-month'),
            year = $(".visualmonthyear").attr('data-year');          
             $scope.getScheduleList(month, year);
         });
    };
    $scope.getScheduleList = function(month, year){          
        $.get(SfService.getUrl("_schlist"), {
            month: month, year: year
        }, function(data) {
            var obj = JSON.parse(data);        
            $("#schedule-list").empty();
            $.each(obj, function(k,v){
                $("#schedule-list").append('<a href="#" class="list-group-item text-ellipsis">\
                    <span class="badge badge-success"> Tgl '+ moment(v.tgl).format('DD')+'</span>'+ v.aktifitas +'</a>');
            });
        });
    };
    
    
    
    //$scope.handleScheduleCalendar();
    //$scope.getScheduleList(thismonth, thisyear);

}]);
</script>
<link href="{{url('coloradmin')}}/assets/plugins/bootstrap-calendar/css/bootstrap_calendar.css" rel="stylesheet" />
@endsection