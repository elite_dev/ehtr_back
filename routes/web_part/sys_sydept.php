<?php
Route::group(['namespace' => 'Sys', 'middleware' => ['web', 'auth']], function () {
	Route::resource('/sys_sydept', 'SydeptController');
	Route::get('/sys_sydept_list', 'SydeptController@getList');
	Route::get('/sys_sydept_lookup', 'SydeptController@getLookup');
});