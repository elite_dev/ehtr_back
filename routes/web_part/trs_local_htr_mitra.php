<?php
Route::group(['namespace' => 'Trs\Local', 'middleware' => ['web', 'auth']], function () {
	Route::resource('/trs_local_htr_mitra', 'htr_mitraController');
	Route::get('/trs_local_htr_mitra_list', 'htr_mitraController@getList');
	Route::get('/trs_local_htr_mitra_lookup', 'htr_mitraController@getLookup');
	Route::get('/trs_local_htr_mitra_get_provinsi', 'htr_mitraController@getProvinsi');
	Route::get('/trs_local_htr_mitra_set_provinsi', 'htr_mitraController@setProvinsi');
	Route::get('/trs_local_htr_mitra_get_kab', 'htr_mitraController@getKab');
	Route::get('/trs_local_htr_mitra_set_kab', 'htr_mitraController@setKab');
	Route::get('/trs_local_htr_mitra_get_kec', 'htr_mitraController@getKec');
	Route::get('/trs_local_htr_mitra_set_kec', 'htr_mitraController@setKec');
	Route::get('/trs_local_htr_mitra_get_desa', 'htr_mitraController@getDesa');
	Route::get('/trs_local_htr_mitra_set_desa', 'htr_mitraController@setDesa');
	Route::get('/trs_local_htr_mitra_auto_calon', 'htr_mitraController@autoCalon');
});