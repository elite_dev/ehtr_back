<?php
Route::group(['namespace' => 'Sys', 'middleware' => ['web', 'auth']], function () {
	Route::resource('/sys_sydump', 'SydumpController');
	Route::get('/sys_sydump_list', 'SydumpController@getList');
	Route::get('/sys_sydump_lookup', 'SydumpController@getLookup');
	Route::post('/sys_sydump_backup', 'SydumpController@backup');
});