<?php
Route::group(['namespace' => 'Sys', 'middleware' => ['web', 'auth']], function () {
	Route::resource('/sys_sycalendar', 'SycalendarController');
	Route::get('/sys_sycalendar_list', 'SycalendarController@getList');
	Route::get('/sys_sycalendar_listCalendar', 'SycalendarController@getListCalendar');
	Route::get('/sys_sycalendar_lookup', 'SycalendarController@getLookup');
});