<?php
Route::group(['namespace' => 'Sys', 'middleware' => ['web', 'auth']], function () {
	Route::resource('/sys_sybot', 'SybotController');
	Route::get('/sys_sybot_list', 'SybotController@getList');
	Route::get('/sys_sybot_lookup', 'SybotController@getLookup');
});