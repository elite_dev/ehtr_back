<?php
Route::group(['namespace' => 'Sys', 'middleware' => ['web', 'auth']], function () {
	Route::resource('/sys_syrpt', 'SyrptController');
	Route::get('/sys_syrpt_list', 'SyrptController@getList');
	Route::get('/sys_syrpt_lookup', 'SyrptController@getLookup');
	Route::post('/sys_syrpt_simulate', 'SyrptController@simulate');
	Route::get('/sys_syrpt_run_table', 'SyrptController@runTable');
	Route::get('/sys_syrpt_rpt01', 'SyrptController@rpt01');
});