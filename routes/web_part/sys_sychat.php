<?php
Route::group(['namespace' => 'Sys', 'middleware' => ['web', 'auth']], function () {
	Route::resource('/sys_sychat', 'SychatController');
	Route::get('/sys_sychat_list', 'SychatController@getList');
	Route::get('/sys_sychat_lookup', 'SychatController@getLookup');
	Route::get('/sys_sychat_list_chat', 'SychatController@getListChat');
	Route::get('/sys_sychat_add_chat', 'SychatController@addChat');
	Route::get('/sys_sychat_del_chat', 'SychatController@delChat');
});