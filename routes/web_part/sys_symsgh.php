<?php
Route::group(['namespace' => 'Sys', 'middleware' => ['web', 'auth']], function () {
	Route::resource('/sys_symsgh', 'SymsghController');
	Route::get('/sys_symsgh_list', 'SymsghController@getList');
	Route::get('/sys_symsgh_lookup', 'SymsghController@getLookup');
});