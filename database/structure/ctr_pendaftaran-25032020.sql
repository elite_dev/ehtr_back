/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 10.1.39-MariaDB : Database - ctraveldb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `ctr_pendaftaran` */

DROP TABLE IF EXISTS `ctr_pendaftaran`;

CREATE TABLE `ctr_pendaftaran` (
  `no_pendaftaran` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'No Pendaftaran',
  `tgl_pendaftaran` date DEFAULT NULL COMMENT 'Tanggal Pendaftaran',
  `nik` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'NIK',
  `id_paket` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ID Paket',
  `uang_muka` double DEFAULT NULL COMMENT 'Uang Muka',
  `no_paspor` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'No Paspor',
  `tgl_update_paspor` date DEFAULT NULL COMMENT 'Tgl Update Paspor',
  `no_medical` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'No Medical',
  `tgl_update_medical` date DEFAULT NULL COMMENT 'Tgl Update Medical',
  `no_manasik` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'No Manasik',
  `tgl_update_manasik` date DEFAULT NULL COMMENT 'Tgl Update Manasik',
  `status_keuanganan` int(1) DEFAULT '0' COMMENT 'Status Keuangan',
  `no_pelunasan` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'No Pelunasan',
  `tgl_pelunasan` date DEFAULT NULL COMMENT 'Tgl Pelunasan',
  `created_by` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Created By',
  `created_at` datetime DEFAULT NULL COMMENT 'Created At',
  `updated_by` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Updated By',
  `updated_at` datetime DEFAULT NULL COMMENT 'Updated At',
  `deleted_at` datetime DEFAULT NULL COMMENT 'Deleted At',
  PRIMARY KEY (`no_pendaftaran`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `ctr_pendaftaran` */

insert  into `ctr_pendaftaran`(`no_pendaftaran`,`tgl_pendaftaran`,`nik`,`id_paket`,`uang_muka`,`no_paspor`,`tgl_update_paspor`,`no_medical`,`tgl_update_medical`,`no_manasik`,`tgl_update_manasik`,`status_keuanganan`,`no_pelunasan`,`tgl_pelunasan`,`created_by`,`created_at`,`updated_by`,`updated_at`,`deleted_at`) values 
('REG2002000','2020-02-23','3374100302900003','CTR2020001',5000000,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'admin','2020-02-23 23:02:14',NULL,'2020-02-23 23:39:25',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
