/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 10.1.39-MariaDB : Database - ctraveldb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `ctr_paspor` */

DROP TABLE IF EXISTS `ctr_paspor`;

CREATE TABLE `ctr_paspor` (
  `no_paspor` varchar(30) CHARACTER SET latin1 NOT NULL COMMENT 'No Paspor',
  `no_pendaftaran` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'No Pendaftaran',
  `tgl_berangkat` date DEFAULT NULL COMMENT 'Tgl Berangkat',
  `nik` varchar(30) CHARACTER SET latin1 NOT NULL COMMENT 'NIK',
  `nama` varchar(100) CHARACTER SET latin1 DEFAULT NULL COMMENT 'Nama',
  `kewarganegaraan` varchar(45) CHARACTER SET latin1 DEFAULT NULL COMMENT 'Kewarganegaraan',
  `tgl_berlaku` date DEFAULT NULL COMMENT 'Tgl Berlaku',
  `tgl_expired` date DEFAULT NULL COMMENT 'Tgl Expired',
  `tgl_lahir` date DEFAULT NULL COMMENT 'Tgl Lahir',
  `tempat_lahir` varchar(50) CHARACTER SET latin1 DEFAULT NULL COMMENT 'Tempat Lahir',
  `no_register` varchar(30) CHARACTER SET latin1 DEFAULT NULL COMMENT 'No Register',
  `jenis` varchar(1) CHARACTER SET latin1 DEFAULT NULL COMMENT 'Jenis',
  `kantor` varchar(50) CHARACTER SET latin1 DEFAULT NULL COMMENT 'Kantor',
  `jenis_kelamin` varchar(5) CHARACTER SET latin1 DEFAULT NULL COMMENT 'Jenis Kelamin',
  `foto` text CHARACTER SET latin1 COMMENT 'Foto',
  `dokumen` text CHARACTER SET latin1 COMMENT 'Dokumen',
  `validate` int(1) DEFAULT '0' COMMENT 'Validate',
  `status` int(1) DEFAULT '0' COMMENT 'Status',
  `created_by` varchar(10) CHARACTER SET latin1 DEFAULT NULL COMMENT 'Created By',
  `updated_by` varchar(10) CHARACTER SET latin1 DEFAULT NULL COMMENT 'Updated By',
  `created_at` datetime DEFAULT NULL COMMENT 'Created At',
  `updated_at` datetime DEFAULT NULL COMMENT 'Updated At',
  `deleted_at` datetime DEFAULT NULL COMMENT 'Deleted At',
  PRIMARY KEY (`no_paspor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `ctr_paspor` */

insert  into `ctr_paspor`(`no_paspor`,`no_pendaftaran`,`tgl_berangkat`,`nik`,`nama`,`kewarganegaraan`,`tgl_berlaku`,`tgl_expired`,`tgl_lahir`,`tempat_lahir`,`no_register`,`jenis`,`kantor`,`jenis_kelamin`,`foto`,`dokumen`,`validate`,`status`,`created_by`,`updated_by`,`created_at`,`updated_at`,`deleted_at`) values 
('0001','REG2002000',NULL,'3374100302900003','ARMANSYAH PUTRA PRATAMA UNO','WNI','2020-03-13','2021-03-13','1990-02-02','TEMANGGUNG','PASPOR001','P','KANTOR JAKARTA','L','uploads/trs_local_ctr_datapribadi/3374100302900003/WIN_20190822_13_03_38_Pro.jpg','PASPOR',1,1,'admin',NULL,'2020-03-13 23:42:54','2020-03-16 22:49:28',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
