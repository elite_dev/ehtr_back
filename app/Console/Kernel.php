<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {
	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		//
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule) {
		$schedule->exec("Test Scheduler Jam  " . date('Y-m-d H:i:s'))
			->everyMinute();

		$schedule->call(function () {
			$c = new \App\Http\Controllers\Trs\Local\NsapsohController();
			$c->doSapSync('date', '1201', '', date('Y-m-d', strtotime("-3 months")), date('Y-m-d'));
		})
			->hourly();

		/*WOS SCHEDULER*/
		$schedule->call(function () {
			$c = new \App\Http\Controllers\Trs\Local\NwoshController();
			$c->generateSchedulerWos('all', '1201', date('Y-m-d'), 1, '1201-BC');
		})->dailyAt('06:50');

		$schedule->call(function () {
			$c = new \App\Http\Controllers\Trs\Local\NwoshController();
			$c->generateSchedulerWos('all', '1201', date('Y-m-d'), 2, '1201-BC');
		})->dailyAt('18:20');

		$schedule->call(function () {
			$c = new \App\Http\Controllers\Trs\Local\NwoshController();
			$c->generateSchedulerWos('all', '1201', date('Y-m-d'), 1, '1201-BCX');
		})->dailyAt('06:55');

		$schedule->call(function () {
			$c = new \App\Http\Controllers\Trs\Local\NwoshController();
			$c->generateSchedulerWos('all', '1201', date('Y-m-d'), 2, '1201-BCX');
		})->dailyAt('18:25');

		$schedule->call(function () {
			$c = new \App\Http\Controllers\Trs\Local\NwoshController();
			$c->generateSchedulerWos('all', '1201', date('Y-m-d'), 1, '1201-KD');
		})->dailyAt('06:55');

		$schedule->call(function () {
			$c = new \App\Http\Controllers\Trs\Local\NwoshController();
			$c->generateSchedulerWos('all', '1201', date('Y-m-d'), 2, '1201-KD');
		})->dailyAt('18:25');
	}

	/**
	 * Register the commands for the application.
	 *
	 * @return void
	 */
	protected function commands() {
		$this->load(__DIR__ . '/Commands');

		require base_path('routes/console.php');
	}
}
