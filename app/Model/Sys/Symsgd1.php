<?php
namespace App\Model\Sys;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Symsgd1 extends Model {

	use SoftDeletes;

	protected $connection = 'mysql';
	public $incrementing = true;
	public $timestamps = false;
	protected $hidden = [];
	protected $dates = ['deleted_at'];
	protected $table = 'symsgd1';
	protected $primaryKey = "id";
	protected $fillable = [
		'id',
		'msg_id',
		'tocc',
		'userid',
		'email',
		'status',
		'deleted_at',
	];

	public function rel_created_by() {
		return $this->belongsTo('App\Model\Sys\Syuser', 'created_by');
	}

	public function rel_userid() {
		return $this->belongsTo('App\Model\Sys\Syuser', 'userid');
	}

}
