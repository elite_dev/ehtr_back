<?php
namespace App\Model\Sys;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sydump extends Model {

	use SoftDeletes;

	protected $connection = 'mysql';
	public $incrementing = true;
	public $timestamps = true;
	protected $hidden = [];
	protected $dates = ['deleted_at'];
	protected $table = 'sydump';
	protected $primaryKey = "id";
	protected $fillable = [
		'id',
		'db_host',
		'db_name',
		'db_user',
		'db_pass',
		'config',
		'schedule_type',
		'schedule_time',
		'output_type',
		'output_path',
		'note',
		'created_by',
		'created_at',
		'updated_at',
		'deleted_at',
	];

	public function rel_created_by() {
		return $this->belongsTo('App\Model\Sys\Syuser', 'created_by');
	}

}
