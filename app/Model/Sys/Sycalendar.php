<?php
namespace App\Model\Sys;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sycalendar extends Model {

	use SoftDeletes;

	protected $connection = 'mysql';
	public $incrementing = true;
	public $timestamps = true;
	protected $hidden = [];
	protected $dates = ['deleted_at'];
	protected $table = 'sycalendar';
	protected $primaryKey = "id";
	protected $fillable = [
		'id',
		'userid',
		'plant',
		'subj',
		'note',
		'style',
		'start_at',
		'finish_at',
		'created_by',
		'created_at',
		'updated_at',
		'deleted_at',
	];

	public function rel_created_by() {
		return $this->belongsTo('App\Model\Sys\Syuser', 'created_by');
	}

}
