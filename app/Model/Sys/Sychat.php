<?php
namespace App\Model\Sys;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sychat extends Model {

	use SoftDeletes;

	protected $connection = 'mysql';
	public $incrementing = true;
	public $timestamps = true;
	protected $hidden = [];
	protected $dates = ['deleted_at'];
	protected $table = 'sychat';
	protected $primaryKey = "id";
	protected $fillable = [
		'id',
		'ctg',
		'doc_no',
		'parent_id',
		'msg_type',
		'msg',
		'created_by',
		'created_at',
		'updated_at',
		'deleted_at',
	];

	public function rel_created_by() {
		return $this->belongsTo('App\Model\Sys\Syuser', 'created_by');
	}

}
