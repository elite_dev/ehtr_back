<?php
namespace App\Model\Sys;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Symsgh extends Model {

	use SoftDeletes;

	protected $connection = 'mysql';
	public $incrementing = true;
	public $timestamps = true;
	protected $hidden = [];
	protected $dates = ['deleted_at'];
	protected $table = 'symsgh';
	protected $primaryKey = "msg_id";
	protected $fillable = [
		'msg_id',
		'msg_type',
		'npk_from',
		'subj',
		'body',
		'msg_mode',
		'ref_id',
		'created_by',
		'created_at',
		'updated_at',
		'deleted_at',
	];

	public function rel_created_by() {
		return $this->belongsTo('App\Model\Sys\Syuser', 'created_by');
	}
	public function rel_npk_from() {
		return $this->belongsTo('App\Model\Sys\Syuser', 'npk_from');
	}
	public function rel_symsgd1() {
		return $this->hasMany('App\Model\Sys\Symsgd1', 'msg_id');
	}

}
