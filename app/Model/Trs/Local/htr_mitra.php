<?php
namespace App\Model\Trs\Local;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class htr_mitra extends Model {

	use SoftDeletes;

	protected $connection = 'mysql';
	public $incrementing = false;
	public $timestamps = true;
	protected $hidden = [];
	protected $dates = ['deleted_at'];
	protected $table = 'htr_mitra';
	protected $primaryKey = "idmitra";
	protected $fillable = [
		'idmitra',
		'nama',
		'group',
		'jabatan',
		'alamat',
		'rtrw',
		'dusun',
		'desa',
		'kecamatan',
		'kabupaten',
		'provinsi',
		'no_ktp',
		'jenis_kelamin',
		'no_telp',
		'no_hp',
		'no_wa',
		'email',
		'foto',
		'created_by',
		'updated_by',
		'created_at',
		'updated_at',
		'deleted_at',
	];

	public function rel_created_by() {
		return $this->belongsTo('App\Model\Sys\Syuser', 'created_by');
	}

	public function rel_mprovinces(){
		return $this->belongsTo('App\Model\Trs\Local\Mprovinces', 'provinsi', 'id');
	}

	public function rel_mregencies(){
		return $this->belongsTo('App\Model\Trs\Local\Mregencies', 'kabupaten', 'id');
	}

	public function rel_mdistricts(){
		return $this->belongsTo('App\Model\Trs\Local\Mdistricts', 'kecamatan', 'id');
	}

	public function rel_mvillages(){
		return $this->belongsTo('App\Model\Trs\Local\Mvillages', 'desa', 'id');
	}
}
