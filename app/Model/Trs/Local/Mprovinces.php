<?php
namespace App\Model\Trs\Local;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mprovinces extends Model {

	use SoftDeletes;

	protected $connection = 'mysql';
	public $incrementing = false;
	public $timestamps = false;
	protected $hidden = [];
	protected $dates = ['deleted_at'];
	protected $table = 'mprovinces';
	protected $primaryKey = "id";
	protected $fillable = [
		'id',
		'name',
		'deleted_at',
	];

	public function rel_created_by() {
		return $this->belongsTo('App\Model\Sys\Syuser', 'created_by');
	}

}
