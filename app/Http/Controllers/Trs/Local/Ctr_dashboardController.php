<?php
namespace App\Http\Controllers\Trs\Local;

use App\Http\Controllers\Controller;
use App\Model\Trs\Local\Ctr_jadwalh;
use App\Model\Trs\Local\Ctr_jadwald1;
use App\Model\Trs\Local\Ctr_jadwald2;
use App\Model\Trs\Local\Ctr_pendaftaran;
use Auth;
use DB;
use App\Sf;
use Illuminate\Http\Request;

class Ctr_dashboardController extends Controller {

	public function index(Request $request) {
		if (!$plant = Sf::isPlant()) {
			return Sf::selectPlant();
		}

		Sf::log("trs_local_ctr_jadwalh", "Ctr_jadwalhController@" . __FUNCTION__, "Open Page  ", "link");

		return view('trs.local.ctr_jadwalh.ctr_jadwalh_frm', compact(['request','plant']));
	}

	public function getList(Request $request) {
		if (!Sf::allowed('TRS_LOCAL_CTR_JADWALH_R')) {
			return response()->json(Sf::reason(), 401);
		}
		$data = Ctr_jadwalh::where(function ($q) use ($request) {
			$q->orWhere('id_jadwal', 'like', "%" . @$request->q . "%");
			$q->orWhere('aktifitas', 'like', "%" . @$request->q . "%");
		})
			//->where('plant',$request->plant)
			->orderBy(isset($request->order_by) ? substr($request->order_by, 1) : 'id_jadwal', substr(@$request->order_by, 0, 1) == '-' ? 'desc' : 'asc');
			if ($request->trash == 1) {
				$data = $data->onlyTrashed();
			}
			$data = $data->paginate(isset($request->limit) ? $request->limit : 10);
		return response()->json(compact(['data']));
	}
	public function getCalendar(Request $request){
		if (!Sf::allowed('TRS_LOCAL_CTR_JADWALH_R')) {
			return response()->json(Sf::reason(), 401);
		}		
		$data = Ctr_jadwald2::select(DB::raw('tgl, aktifitas'))
		->whereMonth('tgl', '=', $request->month)
		->whereYear('tgl', '=', $request->year)
		->orderBy('tgl', 'asc')->get();
		$eventsJson = array();
		foreach ($data as $event) {            
            $eventsJson[] = array(
                date('j/n/Y', strtotime($event->tgl)),  
                '' ,             
                '#00acac',
                '#00acac',                
                $event->aktifitas
            );
        };      
		return response()->json($eventsJson);	
	}
	public function getScheduleList(Request $request){
		if (!Sf::allowed('TRS_LOCAL_CTR_JADWALH_R')) {
			return response()->json(Sf::reason(), 401);
		}		
		$data = Ctr_jadwald2::select(DB::raw('tgl, aktifitas'))
		->whereMonth('tgl', '=', $request->month)
		->whereYear('tgl', '=', $request->year)
		->orderBy('tgl', 'asc')->get();		     
		return json_encode($data);	
	}
	public function getNewRegistered(Request $request){
		if (!Sf::allowed('TRS_LOCAL_CTR_PENDAFTARAN_R')) {
			return response()->json(Sf::reason(), 401);
		}
		$data = Ctr_pendaftaran::with(['rel_paket'])
			->with(['rel_datapribadi'])			
			->orderBy('created_at', 'desc')->get();				
		return json_encode($data);	
	}
}