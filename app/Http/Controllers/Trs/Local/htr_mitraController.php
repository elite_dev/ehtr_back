<?php
namespace App\Http\Controllers\Trs\Local;

use App\Http\Controllers\Controller;
use App\Model\Trs\Local\htr_mitra;
use App\Model\Trs\Local\Mdistricts;
use App\Model\Trs\Local\Mprovinces;
use App\Model\Trs\Local\Mregencies;
use App\Model\Trs\Local\Mvillages;
use Auth;
use DB;
use App\Sf;
use Illuminate\Http\Request;

class htr_mitraController extends Controller {

	public function index(Request $request) {
		if (!$plant = Sf::isPlant()) {
			return Sf::selectPlant();
		}

		Sf::log("trs_local_htr_mitra", "htr_mitraController@" . __FUNCTION__, "Open Page  ", "link");

		$kab = Mregencies::orderBy('name')->get();
		foreach ($kab as $k => $v) {
			$name2 = explode(" ", $v->name);

			$kab[$k]['name2'] = @$name2[1] . " " . @$name2[2] . " " . @$name2[3];
		}
		return view('trs.local.htr_mitra.htr_mitra_frm', compact(['request', 'plant', 'kab']));
	}

	public function getList(Request $request) {
		
		$data = htr_mitra::where(function ($q) use ($request) {
			$q->orWhere('idmitra', 'like', "%" . @$request->q . "%");
			$q->orWhere('nama', 'like', "%" . @$request->q . "%");
			$q->orWhere('group', 'like', "%" . @$request->q . "%");
			$q->orWhere('alamat', 'like', "%" . @$request->q . "%");
			$q->orWhere('rtrw', 'like', "%" . @$request->q . "%");		
			$q->orWhere('desa', 'like', "%" . @$request->q . "%");
			$q->orWhere('kecamatan', 'like', "%" . @$request->q . "%");
			$q->orWhere('kabupaten', 'like', "%" . @$request->q . "%");
			$q->orWhere('provinsi', 'like', "%" . @$request->q . "%");
			$q->orWhere('no_telp', 'like', "%" . @$request->q . "%");
			$q->orWhere('no_hp', 'like', "%" . @$request->q . "%");
			$q->orWhere('no_wa', 'like', "%" . @$request->q . "%");
			$q->orWhere('email', 'like', "%" . @$request->q . "%");
		})
			//->where('plant',$request->plant)
			->orderBy(isset($request->order_by) ? substr($request->order_by, 1) : 'idmitra', substr(@$request->order_by, 0, 1) == '-' ? 'desc' : 'asc');
			if ($request->trash == 1) {
				$data = $data->onlyTrashed();
			}
			$data = $data->paginate(isset($request->limit) ? $request->limit : 10);
		return response()->json(compact(['data']));
	}

	public function getLookup(Request $request) {
		$data = htr_mitra::where(function ($q) use ($request) {
			$q->orWhere('idmitra', 'like', "%" . @$request->q . "%");
			$q->orWhere('nama', 'like', "%" . @$request->q . "%");
			$q->orWhere('alamat', 'like', "%" . @$request->q . "%");
			$q->orWhere('rtrw', 'like', "%" . @$request->q . "%");
			$q->orWhere('desa', 'like', "%" . @$request->q . "%");
			$q->orWhere('kecamatan', 'like', "%" . @$request->q . "%");
			$q->orWhere('kabupaten', 'like', "%" . @$request->q . "%");
			$q->orWhere('provinsi', 'like', "%" . @$request->q . "%");
			$q->orWhere('no_telp', 'like', "%" . @$request->q . "%");
			$q->orWhere('no_hp', 'like', "%" . @$request->q . "%");
			$q->orWhere('no_wa', 'like', "%" . @$request->q . "%");
			$q->orWhere('email', 'like', "%" . @$request->q . "%");
		})
			//->where('plant',$request->plant)
			->orderBy(isset($request->order_by) ? substr($request->order_by, 1) : 'idmitra', substr(@$request->order_by, 0, 1) == '-' ? 'desc' : 'asc');
			$data = $data->paginate(isset($request->limit) ? $request->limit : 10);
		return view('sys.system.dialog.sflookup', compact(['data', 'request']));
	}

	public function store(Request $request) {
		$req = json_decode(request()->getContent());
		$h = $req->h;
		$f = $req->f;
		$h->tanggal_lahir = date('Y/m/d', strtotime($h->tanggal_lahir));
		$h->tgl_berlaku = date('Y/m/d', strtotime($h->tgl_berlaku));
		try {
			$arr = array_merge((array) $h, ['plant'=>$f->plant,'updated_at' => date('Y-m-d H:i:s')]);
			if ($f->crud == 'c') {
				if (!Sf::allowed('trs_local_htr_mitra_C')) {
					return response()->json(Sf::reason(), 401);
				}
				$data = new htr_mitra();
				$arr = array_merge($arr, ['created_by' => Auth::user()->userid, 'created_at' => date('Y-m-d H:i:s')]);
				$data->create($arr);
				$id=$h->idmitra;
				Sf::log("trs_local_htr_mitra", $id, "Create Data Penanam (htr_mitra) idmitra : " . $id, "create");
				return response()->json('created');
			} else {
				if (!Sf::allowed('trs_local_htr_mitra_U')) {
					return response()->json(Sf::reason(), 401);
				}
				$data = htr_mitra::find($h->idmitra);
				$data->update($arr);
				$id = $data->idmitra;
				Sf::log("trs_local_htr_mitra", $id, "Update Data Penanam (htr_mitra) idmitra : " . $id, "update");
				return response()->json('updated');
			}

		} catch (\Exception $e) {
			return response()->json($e->getMessage(), 500);
		}
	}

	public function edit($id) {
		$h = htr_mitra::where('idmitra', $id)->with(['rel_mprovinces','rel_mregencies','rel_mdistricts','rel_mvillages'])->withTrashed()->first();
		return response()->json(compact(['h']));
	}

	public function destroy($id,Request $request) {
		try {
			$data = htr_mitra::where('idmitra', $id)->withTrashed()->first();
			if ($request->restore == 1) {
				if (!Sf::allowed('trs_local_htr_mitra_S')) {
					return response()->json(Sf::reason(), 401);
				}
				$data->restore();
				Sf::log("trs_local_htr_mitra", $id, "Restore Data Penanam (htr_mitra) idmitra : " . $id, "restore");
				return response()->json('restored');
			} else {
				if (!Sf::allowed('trs_local_htr_mitra_D')) {
					return response()->json(Sf::reason(), 401);
				}
				$data->delete();
				Sf::log("trs_local_htr_mitra", $id, "Delete Data Penanam (htr_mitra) idmitra : " . $id, "delete");
				return response()->json('deleted');
			}
		} catch (\Exception $e) {
			return response()->json($e->getMessage(), 500);
		}
	}

	public function getProvinsi(Request $request){
		$data = Mprovinces::where('name', 'like', "%" . @$request->q . "%")
			->orderBy('id')
			->limit(isset($request->limit) ? $request->limit : 10)->get();
		foreach ($data as $k => $v) {
			$data[$k]->value = $v->id;
			$data[$k]->html = $v->name . " | (" . $v->id . ")";
		}
		return response()->json($data);
	}

	public function setProvinsi(Request $request) {
		$data = Mprovinces::where('id', $request->id)->first();
		return response()->json(compact(['data']));
	}

	public function getKab(Request $request){
		$data = Mregencies::where('name', 'like', "%" . @$request->q . "%")
			->where('province_id', $request->prov_id)
			->orderBy('id')
			->limit(isset($request->limit) ? $request->limit : 10)->get();
		foreach ($data as $k => $v) {
			$data[$k]->value = $v->id;
			$data[$k]->html = $v->name . " | (" . $v->id . ")";
		}
		return response()->json($data);
	}

	public function setKab(Request $request) {
		$data = Mregencies::where('id', $request->id)->first();
		return response()->json(compact(['data']));
	}

	public function getKec(Request $request){
		$data = Mdistricts::where('name', 'like', "%" . @$request->q . "%")
			->where('regency_id', $request->reg_id)
			->orderBy('id')
			->limit(isset($request->limit) ? $request->limit : 10)->get();
		foreach ($data as $k => $v) {
			$data[$k]->value = $v->id;
			$data[$k]->html = $v->name . " | (" . $v->id . ")";
		}
		return response()->json($data);
	}

	public function setKec(Request $request) {
		$data = Mdistricts::where('id', $request->id)->first();
		return response()->json(compact(['data']));
	}

	public function getDesa(Request $request){
		$data = Mvillages::where('name', 'like', "%" . @$request->q . "%")
			->where('district_id', $request->dis_id)
			->orderBy('id')
			->limit(isset($request->limit) ? $request->limit : 10)->get();
		foreach ($data as $k => $v) {
			$data[$k]->value = $v->id;
			$data[$k]->html = $v->name . " | (" . $v->id . ")";
		}
		return response()->json($data);
	}

	public function setDesa(Request $request) {
		$data = Mvillages::where('id', $request->id)->first();
		return response()->json(compact(['data']));
	}

	public function autoCalon(Request $request) {
		$data = htr_mitra::where('nama', 'like', "%" . @$request->q . "%")
			->orderBy('nama')
			->limit(isset($request->limit) ? $request->limit : 10)->get();
		foreach ($data as $k => $v) {
			$data[$k]->value = $v->idmitra;
			$data[$k]->html = $v->nama . " | idmitra : <b>" . $v->idmitra . "</b> | Alamat : ". $v->alamat ;
		}
		return response()->json($data);
	}
}