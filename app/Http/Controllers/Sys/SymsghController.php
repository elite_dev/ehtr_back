<?php
namespace App\Http\Controllers\Sys;

use App\Http\Controllers\Controller;
use App\Model\Sys\Symsgd1;
use App\Model\Sys\Symsgh;
use App\Sf;
use Auth;
use DB;
use Illuminate\Http\Request;

class SymsghController extends Controller {

	public function index(Request $request) {
		if (!$plant = Sf::isPlant()) {
			return Sf::selectPlant();
		}

		Sf::log("sys_symsgh", "SymsghController@" . __FUNCTION__, "Open Page  ", "link");

		return view('sys.symsgh.symsgh_frm', compact(['request', 'plant']));
	}

	public function getList(Request $request) {
		if (!Sf::allowed('SYS_SYMSGH_R')) {
			return response()->json(Sf::reason(), 401);
		}
		$data = Symsgh::where(function ($q) use ($request) {
			$q->orWhere('npk_from', 'like', "%" . @$request->q . "%");
			$q->orWhere('subj', 'like', "%" . @$request->q . "%");
		})->with(['rel_npk_from', 'rel_symsgd1' => function ($q) {
			$q->with('rel_userid');
		}])
			->orderBy(isset($request->order_by) ? substr($request->order_by, 1) : 'msg_id', substr(@$request->order_by, 0, 1) == '-' ? 'desc' : 'asc');

		if ($request->dir == 'inbox') {
			$data = $data->whereHas('rel_symsgd1', function ($q) use ($request) {
				$q->where('userid', $request->userid);
			});
		}
		if ($request->dir == 'sent') {
			$data = $data->where('npk_from', Auth::user()->userid);
		}
		if ($request->dir == 'trash') {
			$data = $data->onlyTrashed()
				->whereHas('rel_symsgd1', function ($q) use ($request) {
					$q->where('userid', $request->userid);
				});
		}
		$data = $data->paginate(isset($request->limit) ? $request->limit : 10);
		return response()->json(compact(['data']));
	}

	public function getLookup(Request $request) {
		$data = Symsgh::where(function ($q) use ($request) {
			$q->orWhere('msg_id', 'like', "%" . @$request->q . "%");
			$q->orWhere('msg_type', 'like', "%" . @$request->q . "%");
			$q->orWhere('npk_from', 'like', "%" . @$request->q . "%");
			$q->orWhere('subj', 'like', "%" . @$request->q . "%");
			$q->orWhere('body', 'like', "%" . @$request->q . "%");
			$q->orWhere('msg_mode', 'like', "%" . @$request->q . "%");
			$q->orWhere('ref_id', 'like', "%" . @$request->q . "%");
		})
			->orderBy(isset($request->order_by) ? substr($request->order_by, 1) : 'msg_id', substr(@$request->order_by, 0, 1) == '-' ? 'desc' : 'asc');
		$data = $data->paginate(isset($request->limit) ? $request->limit : 10);
		return view('sys.system.dialog.sflookup', compact(['data', 'request']));
	}

	public function store(Request $request) {
		$req = json_decode(request()->getContent());
		$h = $req->h;
		$d = $req->d;
		$f = $req->f;

		try {
			$arr = array_merge((array) $h, ['updated_at' => date('Y-m-d H:i:s')]);
			if ($f->crud == 'c') {
				if (!Sf::allowed('SYS_SYMSGH_C')) {
					return response()->json(Sf::reason(), 401);
				}
				$data = new Symsgh();
				$arr = array_merge($arr, ['created_by' => Auth::user()->userid, 'created_at' => date('Y-m-d H:i:s')]);
				$data->create($arr);
				$id = DB::getPdo()->lastInsertId();
				$this->saveDet1($id, $h, $f, $d);
				Sf::log("sys_symsgh", $id, "Create Menu (symsgh) msg_id : " . $id, "create");
				return response()->json('created');
			} else {
				if (!Sf::allowed('SYS_SYMSGH_U')) {
					return response()->json(Sf::reason(), 401);
				}
				$data = Symsgh::find($h->msg_id);
				$data->update($arr);
				$id = $data->msg_id;
				$this->saveDet1($id, $h, $f, $d);
				Sf::log("sys_symsgh", $id, "Update Menu (symsgh) msg_id : " . $id, "update");
				return response()->json('updated');
			}

		} catch (\Exception $e) {
			return response()->json($e->getMessage(), 500);
		}
	}

	private function saveDet1($id, $h, $f, $d) {
		foreach ($d as $k => $v) {
			$data = new Symsgd1();
			$data->msg_id = $id;
			$data->tocc = @$v->tocc;
			$data->userid = @$v->userid;
			$data->email = "";
			$data->status = 0;
			$data->save();
		}
	}

	public function edit($id) {
		$h = Symsgh::where('msg_id', $id)->withTrashed()
			->with(['rel_npk_from', 'rel_symsgd1' => function ($q) {
				$q->with(['rel_userid']);
			}]
			)
			->first();
		return response()->json(compact(['h']));
	}

	public function destroy($id, Request $request) {
		try {
			$data = Symsgh::where('msg_id', $id)->withTrashed()->first();
			if ($request->restore == 1) {
				if (!Sf::allowed('SYS_SYMSGH_S')) {
					return response()->json(Sf::reason(), 401);
				}
				$data->restore();
				Sf::log("sys_symsgh", $id, "Restore Menu (symsgh) msg_id : " . $id, "restore");
				return 'restored';
			} else {
				if (!Sf::allowed('SYS_SYMSGH_D')) {
					return response()->json(Sf::reason(), 401);
				}
				$data->delete();
				Sf::log("sys_symsgh", $id, "Delete Menu (symsgh) msg_id : " . $id, "delete");
				return 'deleted';
			}
		} catch (\Exception $e) {
			return response()->json($e->getMessage(), 500);
		}
	}
}