<?php
namespace App\Http\Controllers\Sys;

use App\Http\Controllers\Controller;
use App\Model\Sys\Sycalendar;
use App\Sf;
use Auth;
use DB;
use Illuminate\Http\Request;

class SycalendarController extends Controller {

	public function index(Request $request) {
		if (!$plant = Sf::isPlant()) {
			return Sf::selectPlant();
		}

		Sf::log("sys_sycalendar", "SycalendarController@" . __FUNCTION__, "Open Page  ", "link");

		return view('sys.sycalendar.sycalendar_frm', compact(['request', 'plant']));
	}

	public function getListCalendar(Request $request) {
		$data = Sycalendar::where('userid', $request->userid)
			->where('plant', $request->plant)
			->get();
		$arr = [];
		foreach ($data as $k => $v) {
			$arr[] = [
				'_id' => $v->id,
				'title' => $v->subj,
				'start' => date("Y-m-d", strtotime($v->start_at)),
				'end' => date("Y-m-d", strtotime($v->finish_at)),
				'color' => $v->style,
			];
		}
		return response()->json(compact(['arr']));
	}
	public function getList(Request $request) {
		if (!Sf::allowed('SYS_SYCALENDAR_R')) {
			return response()->json(Sf::reason(), 401);
		}
		$data = Sycalendar::where(function ($q) use ($request) {
			$q->orWhere('id', 'like', "%" . @$request->q . "%");
			$q->orWhere('userid', 'like', "%" . @$request->q . "%");
			$q->orWhere('plant', 'like', "%" . @$request->q . "%");
			$q->orWhere('subj', 'like', "%" . @$request->q . "%");
			$q->orWhere('note', 'like', "%" . @$request->q . "%");
			$q->orWhere('style', 'like', "%" . @$request->q . "%");
		})
			->orderBy(isset($request->order_by) ? substr($request->order_by, 1) : 'id', substr(@$request->order_by, 0, 1) == '-' ? 'desc' : 'asc');
		if ($request->trash == 1) {
			$data = $data->onlyTrashed();
		}
		$data = $data->paginate(isset($request->limit) ? $request->limit : 10);
		return response()->json(compact(['data']));
	}

	public function getLookup(Request $request) {
		$data = Sycalendar::where(function ($q) use ($request) {
			$q->orWhere('id', 'like', "%" . @$request->q . "%");
			$q->orWhere('userid', 'like', "%" . @$request->q . "%");
			$q->orWhere('plant', 'like', "%" . @$request->q . "%");
			$q->orWhere('subj', 'like', "%" . @$request->q . "%");
			$q->orWhere('note', 'like', "%" . @$request->q . "%");
			$q->orWhere('style', 'like', "%" . @$request->q . "%");
		})
			->orderBy(isset($request->order_by) ? substr($request->order_by, 1) : 'id', substr(@$request->order_by, 0, 1) == '-' ? 'desc' : 'asc');
		$data = $data->paginate(isset($request->limit) ? $request->limit : 10);
		return view('sys.system.dialog.sflookup', compact(['data', 'request']));
	}

	public function store(Request $request) {
		$req = json_decode(request()->getContent());
		$h = $req->h;
		$f = $req->f;

		try {
			$data = Sycalendar::find(@$h->id);
			$arr = array_merge((array) $h, ['updated_at' => date('Y-m-d H:i:s')]);
			if ($data == false) {
				if (!Sf::allowed('SYS_SYCALENDAR_C')) {
					return response()->json(Sf::reason(), 401);
				}
				$data = new Sycalendar();
				$arr = array_merge($arr, ['created_by' => Auth::user()->userid, 'created_at' => date('Y-m-d H:i:s')]);
				$data->create($arr);
				$id = DB::getPdo()->lastInsertId();
				Sf::log("sys_sycalendar", $id, "Create Menu (sycalendar) id : " . $id, "create");
				return response()->json('created');
			} else {
				if (!Sf::allowed('SYS_SYCALENDAR_U')) {
					return response()->json(Sf::reason(), 401);
				}
				$data = Sycalendar::find($h->id);
				$data->update($arr);
				$id = $data->id;
				Sf::log("sys_sycalendar", $id, "Update Menu (sycalendar) id : " . $id, "update");
				return response()->json('updated');
			}

		} catch (\Exception $e) {
			return response()->json($e->getMessage(), 500);
		}
	}

	public function edit($id) {
		$h = Sycalendar::where('id', $id)->withTrashed()->first();
		return response()->json(compact(['h']));
	}

	public function destroy($id, Request $request) {
		try {
			$data = Sycalendar::where('id', $id)->withTrashed()->first();
			if ($request->restore == 1) {
				if (!Sf::allowed('SYS_SYCALENDAR_S')) {
					return response()->json(Sf::reason(), 401);
				}
				$data->restore();
				Sf::log("sys_sycalendar", $id, "Restore Menu (sycalendar) id : " . $id, "restore");
				return response()->json('restored');
			} else {
				if (!Sf::allowed('SYS_SYCALENDAR_D')) {
					return response()->json(Sf::reason(), 401);
				}
				$data->delete();
				Sf::log("sys_sycalendar", $id, "Delete Menu (sycalendar) id : " . $id, "delete");
				return response()->json('deleted');
			}
		} catch (\Exception $e) {
			return response()->json($e->getMessage(), 500);
		}
	}
}