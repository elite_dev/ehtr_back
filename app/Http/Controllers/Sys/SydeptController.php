<?php
namespace App\Http\Controllers\Sys;

use App\Http\Controllers\Controller;
use App\Model\Sys\Sydept;
use App\Sf;
use Auth;
use Illuminate\Http\Request;

class SydeptController extends Controller {

	public function index(Request $request) {
		if (!$plant = Sf::isPlant()) {
			return Sf::selectPlant();
		}

		$divisions = Sydept::select('division')->groupBy('division')->get();
		$group_depts = Sydept::select('group_dept')->groupBy('group_dept')->get();

		Sf::log("sys_sydept", "SydeptController@" . __FUNCTION__, "Open Page  ", "link");

		return view('sys.sydept.sydept_frm', compact(['request', 'plant', 'divisions', 'group_depts']));
	}

	public function getList(Request $request) {
		if (!Sf::allowed('SYS_SYDEPT_R')) {
			return response()->json(Sf::reason(), 401);
		}
		$data = Sydept::where(function ($q) use ($request) {
			$q->orWhere('dept', 'like', "%" . @$request->q . "%");
			$q->orWhere('dept_name', 'like', "%" . @$request->q . "%");
			$q->orWhere('division', 'like', "%" . @$request->q . "%");
			$q->orWhere('group_dept', 'like', "%" . @$request->q . "%");
			$q->orWhere('note', 'like', "%" . @$request->q . "%");
		})
			->where('plant', $request->plant)
			->orderBy(isset($request->order_by) ? substr($request->order_by, 1) : 'dept', substr(@$request->order_by, 0, 1) == '-' ? 'desc' : 'asc');
		if ($request->trash == 1) {
			$data = $data->onlyTrashed();
		}
		$data = $data->paginate(isset($request->limit) ? $request->limit : 10);
		return response()->json(compact(['data']));
	}

	public function getLookup(Request $request) {
		$data = Sydept::where(function ($q) use ($request) {
			$q->orWhere('dept', 'like', "%" . @$request->q . "%");
			$q->orWhere('dept_name', 'like', "%" . @$request->q . "%");
			$q->orWhere('division', 'like', "%" . @$request->q . "%");
			$q->orWhere('group_dept', 'like', "%" . @$request->q . "%");
			$q->orWhere('note', 'like', "%" . @$request->q . "%");
		})
			->where('plant', $request->plant)
			->orderBy(isset($request->order_by) ? substr($request->order_by, 1) : 'dept', substr(@$request->order_by, 0, 1) == '-' ? 'desc' : 'asc');
		$data = $data->paginate(isset($request->limit) ? $request->limit : 10);
		return view('sys.system.dialog.sflookup', compact(['data', 'request']));
	}

	public function store(Request $request) {
		$req = json_decode(request()->getContent());
		$h = $req->h;
		$f = $req->f;
		$h->dept = strtoupper($h->dept);

		try {
			$arr = array_merge((array) $h, ['plant' => $f->plant, 'updated_at' => date('Y-m-d H:i:s')]);
			if ($f->crud == 'c') {
				if (!Sf::allowed('SYS_SYDEPT_C')) {
					return response()->json(Sf::reason(), 401);
				}
				$data = new Sydept();
				$arr = array_merge($arr, ['created_by' => Auth::user()->userid, 'created_at' => date('Y-m-d H:i:s')]);
				$data->create($arr);
				$id = $h->dept;
				Sf::log("sys_sydept", $id, "Create Department (sydept) dept : " . $id, "create");
				return response()->json('created');
			} else {
				if (!Sf::allowed('SYS_SYDEPT_U')) {
					return response()->json(Sf::reason(), 401);
				}
				$data = Sydept::find($h->dept);
				$data->update($arr);
				$id = $data->dept;
				Sf::log("sys_sydept", $id, "Update Department (sydept) dept : " . $id, "update");
				return response()->json('updated');
			}

		} catch (\Exception $e) {
			return response()->json($e->getMessage(), 500);
		}
	}

	public function edit($id) {
		$h = Sydept::where('dept', $id)->withTrashed()->first();
		return response()->json(compact(['h']));
	}

	public function destroy($id, Request $request) {
		try {
			$data = Sydept::where('dept', $id)->withTrashed()->first();
			if ($request->restore == 1) {
				if (!Sf::allowed('SYS_SYDEPT_S')) {
					return response()->json(Sf::reason(), 401);
				}
				$data->restore();
				Sf::log("sys_sydept", $id, "Restore Department (sydept) dept : " . $id, "restore");
				return response()->json('restored');
			} else {
				if (!Sf::allowed('SYS_SYDEPT_D')) {
					return response()->json(Sf::reason(), 401);
				}
				$data->delete();
				Sf::log("sys_sydept", $id, "Delete Department (sydept) dept : " . $id, "delete");
				return response()->json('deleted');
			}
		} catch (\Exception $e) {
			return response()->json($e->getMessage(), 500);
		}
	}
}