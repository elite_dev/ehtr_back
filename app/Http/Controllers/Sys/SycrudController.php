<?php

namespace App\Http\Controllers\Sys;

use App\Http\Controllers\Controller;
use App\Sf;
use DB;
use Illuminate\Http\Request;

class SycrudController extends Controller {

	public function index() {
		$request = [];
		$arrConn = [];
		if (!$plant = Sf::isPlant()) {
			return Sf::selectPlant();
		}

		Sf::log("sys_sycrud", "SycrudController@" . __FUNCTION__, "Open Page  ", "link");
		return view('sys.sycrud.sycrud_frm', compact(['request', 'arrConn', 'plant']));
	}

	public function fn($fn, Request $request) {
		return $this->$fn($request);
	}

	public function getDbs(Request $request) {
		$r = json_decode($request->h);
		try {
			$data_dbs = DB::connection($r->schema)->select("SELECT table_schema FROM TABLES GROUP BY table_schema");
			$dbs = [];
			foreach ($data_dbs as $key => $v) {
				$dbs[] = $v->table_schema;
			}
			return response()->json(compact(['dbs']));
		} catch (\Exception $e) {
			return [];
		}
	}

	public function getTables(Request $request) {
		$r = json_decode($request->h);
		try {
			$data_dbs = DB::connection($r->schema)->select("SELECT table_name FROM TABLES a WHERE table_schema='" . $r->db . "' AND table_type='BASE TABLE' GROUP BY table_name");
			$tables = [];
			foreach ($data_dbs as $key => $v) {
				$tables[] = $v->table_name;
			}
			return response()->json(compact(['tables']));
		} catch (\Exception $e) {
			return [];
		}
	}

	public function getFields(Request $request) {
		$r = json_decode($request->h);
		try {
			$data_dbs = DB::connection($r->schema)->select("SELECT * FROM COLUMNS a WHERE table_schema='" . $r->db . "' AND table_name='" . $r->table . "' ");
			$fields = [];
			foreach ($data_dbs as $key => $v) {
				if (in_array($v->COLUMN_NAME, ['created_by', 'updated_by', 'created_at', 'updated_at', 'deleted_at'])) {
					$v->ELEMENT = "none";
				} else if (in_array($v->EXTRA, ['auto_increment'])) {
					$v->ELEMENT = "text readonly";
				} else {
					$v->ELEMENT = "text";
				}
				$v->CAPTION = ucwords(str_replace('_', ' ', $v->COLUMN_NAME));
				$v->REQUIRED = "";
				$fields[] = $v;
			}
			return response()->json(compact(['fields']));
		} catch (\Exception $e) {
			return [];
		}
	}

	public function getScript(Request $request) {
		$req = json_decode(request()->getContent());
		$h = $req->h;
		$s = $req->structure;

		$conf['pathCamel'] = "";
		$conf['pathLower'] = "";
		foreach (explode('\\', $h->path) as $k => $v) {
			$conf['pathCamel'] .= ucwords(strtolower($v)) . '\\';
			$conf['pathLower'] .= strtolower($v) . '\\';
		}
		$conf['tableCamel'] = ucwords(strtolower($h->table));
		$conf['tableLower'] = strtolower($h->table);
		$str = str_replace("\\", "_", $conf['pathLower'] . $conf['tableLower']);
		$conf['route'] = substr($str, 0, strlen($str));
		$conf['namespace'] = substr($conf['pathCamel'], 0, strlen($conf['pathCamel']) - 1);
		// dd($conf);

		$routes = [
			'path' => $this->cleanPath(base_path() . '\\routes\\web_part\\' . str_replace('\\', '_', $conf['pathLower']) . $h->table . '.php'),
			'str' => $this->generateRoutes($h, $s, $conf),
		];
		$model = ['path' => $this->cleanPath(base_path() . '\\app\\Model\\' . $conf['pathCamel'] . ucwords(strtolower($h->table)) . '.php'),
			'str' => $this->generateModel($h, $s, $conf)];
		$controller = ['path' => $this->cleanPath(base_path() . '\\app\\Http\\Controllers\\' . $conf['pathCamel'] . ucwords(strtolower($h->table)) . 'Controller.php'),
			'str' => $this->generateController($h, $s, $conf)];
		$form = ['path' => $this->cleanPath(base_path() . '\\resources\\views\\' . $conf['pathLower'] . '\\' . $h->table . '\\' . $h->table . '_frm.blade.php'),
			'str' => $this->generateForm($h, $s, $conf)];
		$access = ['path' => 'ss', 'str' => 'sttr'];
		return response()->json(compact(['routes', 'model', 'controller', 'form', 'access', 'conf']));

	}

	private function cleanPath($path){
		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
		    $path=str_replace("/", "\\", $path);
		} else {
		    $path=str_replace("\\", "/", $path);
		}
		return response()->json($path);
	}

	private function generateRoutes($h, $s, $conf) {
		return htmlspecialchars(view('sys.sycrud.sycrud_template_routes', compact(['h', 's', 'conf'])));
	}

	private function generateModel($h, $s, $conf) {
		return htmlspecialchars(view('sys.sycrud.sycrud_template_model', compact(['h', 's', 'conf'])));
	}

	private function generateController($h, $s, $conf) {
		return htmlspecialchars(view('sys.sycrud.sycrud_template_controller', compact(['h', 's', 'conf'])));
	}

	private function generateForm($h, $s, $conf) {
		return htmlspecialchars(view('sys.sycrud.sycrud_template_form', compact(['h', 's', 'conf'])));
	}

	public function writeScript(Request $request) {
		if (!Sf::allowed('SYS_SYCRUD_C')) {
			return response()->json(Sf::reason(), 401);
		}
		$req = json_decode(request()->getContent());
		$path = $req->path;
		if (file_exists($path)) {
			return response()->json('File Already Exist', 404);
		} else {
			if (!file_exists(dirname($path))) {
				mkdir(dirname($path), 0755, true);
			}
			$myfile = fopen($path, "w") or die("Unable to open file!");
			fwrite($myfile, html_entity_decode($req->script));
			fclose($myfile);
			Sf::log("sys_sycrud", $req->route, "Write " . $req->id . " CRUD Builder $path", "create");
			return response()->json("Success, File created");
		}

	}

}
