<?php
namespace App\Http\Controllers\Sys;

use App\Http\Controllers\Controller;
use App\Model\Sys\Sybot;
use App\Sf;
use Auth;
use DB;
use Illuminate\Http\Request;

class SybotController extends Controller
{

    public function index(Request $request)
    {
        if (!$plant = Sf::isPlant()) {
            return Sf::selectPlant();
        }

        Sf::log("sys_sybot", "SybotController@" . __FUNCTION__, "Open Page  ", "link");

        return view('sys.sybot.sybot_frm', compact(['request', 'plant']));
    }

    public function getList(Request $request)
    {
        if (!Sf::allowed('SYS_SYBOT_R')) {
            return response()->json(Sf::reason(), 401);
        }
        $data = Sybot::where(function ($q) use ($request) {
            $q->orWhere('id', 'like', "%" . @$request->q . "%");
            $q->orWhere('plant', 'like', "%" . @$request->q . "%");
            $q->orWhere('to_id', 'like', "%" . @$request->q . "%");
            $q->orWhere('msg', 'like', "%" . @$request->q . "%");
            $q->orWhere('status', 'like', "%" . @$request->q . "%");
        })
        //->where('plant',$request->plant)
            ->orderBy(isset($request->order_by) ? substr($request->order_by, 1) : 'id', substr(@$request->order_by, 0, 1) == '-' ? 'desc' : 'asc');
        if ($request->trash == 1) {
            $data = $data->onlyTrashed();
        }
        $data = $data->paginate(isset($request->limit) ? $request->limit : 10);
        return response()->json(compact(['data']));
    }

    public function getLookup(Request $request)
    {
        $data = Sybot::where(function ($q) use ($request) {
            $q->orWhere('id', 'like', "%" . @$request->q . "%");
            $q->orWhere('plant', 'like', "%" . @$request->q . "%");
            $q->orWhere('to_id', 'like', "%" . @$request->q . "%");
            $q->orWhere('msg', 'like', "%" . @$request->q . "%");
            $q->orWhere('status', 'like', "%" . @$request->q . "%");
        })
        //->where('plant',$request->plant)
            ->orderBy(isset($request->order_by) ? substr($request->order_by, 1) : 'id', substr(@$request->order_by, 0, 1) == '-' ? 'desc' : 'asc');
        $data = $data->paginate(isset($request->limit) ? $request->limit : 10);
        return view('sys.system.dialog.sflookup', compact(['data', 'request']));
    }

    private function sendBot($msg)
    {
        /**
        Use this certificate root certificate bundle:
        https://curl.haxx.se/ca/cacert.pem
        Copy this certificate bundle on your disk. And use this on php.ini
        curl.cainfo = "path_to_cert\cacert.pem"
         */
        $endpoint = "https://api.telegram.org/bot699493625:AAFqP9WlBPQbUvJlU0iVp6cy5SnujoAeHqs/sendMessage";
        $client   = new \GuzzleHttp\Client();
        $id       = 274964237;
        $response = $client->request('GET', $endpoint, ['query' => [
            'chat_id' => $id,
            'text'    => $msg,
        ]]);

        $statusCode = $response->getStatusCode();
        $content    = $response->getBody();

        if ($statusCode == 200) {

            return true;
        } else {
            return false;
        }
    }

    public function store(Request $request)
    {
        $req = json_decode(request()->getContent());
        $h   = $req->h;
        $f   = $req->f;

        if (!$this->sendBot($h->msg)) {
            return response()->json("Message sending failure", 500);
        } else {
            $h->status = 1;
        }

        try {
            $arr = array_merge((array) $h, ['plant' => $f->plant, 'updated_at' => date('Y-m-d H:i:s')]);
            if ($f->crud == 'c') {
                if (!Sf::allowed('SYS_SYBOT_C')) {
                    return response()->json(Sf::reason(), 401);
                }
                $data = new Sybot();
                $arr  = array_merge($arr, ['created_by' => Auth::user()->userid, 'created_at' => date('Y-m-d H:i:s')]);
                $data->create($arr);
                $id = DB::getPdo()->lastInsertId();
                Sf::log("sys_sybot", $id, "Create Telegram Bot (sybot) id : " . $id, "create");
                return response()->json('created');
            } else {
                if (!Sf::allowed('SYS_SYBOT_U')) {
                    return response()->json(Sf::reason(), 401);
                }
                $data = Sybot::find($h->id);
                $data->update($arr);
                $id = $data->id;
                Sf::log("sys_sybot", $id, "Update Telegram Bot (sybot) id : " . $id, "update");
                return response()->json('updated');
            }

        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    public function edit($id)
    {
        $h = Sybot::where('id', $id)->withTrashed()->first();
        return response()->json(compact(['h']));
    }

    public function destroy($id, Request $request)
    {
        try {
            $data = Sybot::where('id', $id)->withTrashed()->first();
            if ($request->restore == 1) {
                if (!Sf::allowed('SYS_SYBOT_S')) {
                    return response()->json(Sf::reason(), 401);
                }
                $data->restore();
                Sf::log("sys_sybot", $id, "Restore Telegram Bot (sybot) id : " . $id, "restore");
                return response()->json('restored');
            } else {
                if (!Sf::allowed('SYS_SYBOT_D')) {
                    return response()->json(Sf::reason(), 401);
                }
                $data->delete();
                Sf::log("sys_sybot", $id, "Delete Telegram Bot (sybot) id : " . $id, "delete");
                return response()->json('deleted');
            }
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}
