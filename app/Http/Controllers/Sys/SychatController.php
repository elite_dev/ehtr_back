<?php
namespace App\Http\Controllers\Sys;

use App\Http\Controllers\Controller;
use App\Model\Sys\Sychat;
use App\Sf;
use Auth;
use DB;
use Illuminate\Http\Request;

class SychatController extends Controller {

	public function index(Request $request) {
		if (!$plant = Sf::isPlant()) {
			return Sf::selectPlant();
		}

		Sf::log("sys_sychat", "SychatController@" . __FUNCTION__, "Open Page  ", "link");

		return view('sys.sychat.sychat_frm', compact(['request', 'plant']));
	}

	public function getList(Request $request) {
		if (!Sf::allowed('SYS_SYCHAT_R')) {
			return response()->json(Sf::reason(), 401);
		}
		$data = Sychat::where(function ($q) use ($request) {
			$q->orWhere('ctg', 'like', "%" . @$request->q . "%");
			$q->orWhere('doc_no', 'like', "%" . @$request->q . "%");
			$q->orWhere('msg_type', 'like', "%" . @$request->q . "%");
			$q->orWhere('msg', 'like', "%" . @$request->q . "%");
		})
		//->where('plant',$request->plant)
			->orderBy(isset($request->order_by) ? substr($request->order_by, 1) : 'id', substr(@$request->order_by, 0, 1) == '-' ? 'desc' : 'asc');
		if ($request->trash == 1) {
			$data = $data->onlyTrashed();
		}
		$data = $data->paginate(isset($request->limit) ? $request->limit : 10);
		return response()->json(compact(['data']));
	}

	public function getListChat(Request $request) {
		$data = Sychat::where('ctg', $request->route)
			->where('doc_no', $request->id)
			->withTrashed()
			->with(['rel_created_by'])
			->orderBy('created_at')
			->orderBy('parent_id')->get();
		return response()->json(compact(['data']));
	}

	public function getLookup(Request $request) {
		$data = Sychat::where(function ($q) use ($request) {
			$q->orWhere('ctg', 'like', "%" . @$request->q . "%");
			$q->orWhere('doc_no', 'like', "%" . @$request->q . "%");
			$q->orWhere('msg_type', 'like', "%" . @$request->q . "%");
			$q->orWhere('msg', 'like', "%" . @$request->q . "%");
		})
		//->where('plant',$request->plant)
			->orderBy(isset($request->order_by) ? substr($request->order_by, 1) : 'id', substr(@$request->order_by, 0, 1) == '-' ? 'desc' : 'asc');
		$data = $data->paginate(isset($request->limit) ? $request->limit : 10);
		return view('sys.system.dialog.sflookup', compact(['data', 'request']));
	}

	public function store(Request $request) {
		$req = json_decode(request()->getContent());
		$h = $req->h;
		$f = $req->f;

		try {
			$arr = array_merge((array) $h, ['plant' => $f->plant, 'updated_at' => date('Y-m-d H:i:s')]);
			if ($f->crud == 'c') {
				if (!Sf::allowed('SYS_SYCHAT_C')) {
					return response()->json(Sf::reason(), 401);
				}
				$data = new Sychat();
				$arr = array_merge($arr, ['created_by' => Auth::user()->userid, 'created_at' => date('Y-m-d H:i:s')]);
				$data->create($arr);
				$id = DB::getPdo()->lastInsertId();
				Sf::log("sys_sychat", $id, "Create Comment and Chat (sychat) id : " . $id, "create");
				return response()->json('created');
			} else {
				if (!Sf::allowed('SYS_SYCHAT_U')) {
					return response()->json(Sf::reason(), 401);
				}
				$data = Sychat::find($h->id);
				$data->update($arr);
				$id = $data->id;
				Sf::log("sys_sychat", $id, "Update Comment and Chat (sychat) id : " . $id, "update");
				return response()->json('updated');
			}

		} catch (\Exception $e) {
			return response()->json($e->getMessage(), 500);
		}
	}

	public function edit($id) {
		$h = Sychat::where('id', $id)->withTrashed()->first();
		return response()->json(compact(['h']));
	}

	public function destroy($id, Request $request) {
		try {
			$data = Sychat::where('id', $id)->withTrashed()->first();
			if ($request->restore == 1) {
				if (!Sf::allowed('SYS_SYCHAT_S')) {
					return response()->json(Sf::reason(), 401);
				}
				$data->restore();
				Sf::log("sys_sychat", $id, "Restore Comment and Chat (sychat) id : " . $id, "restore");
				return 'restored';
			} else {
				if (!Sf::allowed('SYS_SYCHAT_D')) {
					return response()->json(Sf::reason(), 401);
				}
				$data->delete();
				Sf::log("sys_sychat", $id, "Delete Comment and Chat (sychat) id : " . $id, "delete");
				return 'deleted';
			}
		} catch (\Exception $e) {
			return response()->json($e->getMessage(), 500);
		}
	}

	public function addChat(Request $request) {
		$data = new Sychat();
		$data->ctg = $request->ctg;
		$data->doc_no = $request->doc_no;
		$data->parent_id = $request->parent_id;
		$data->msg_type = $request->msg_type;
		$data->msg = $request->msg;
		$data->created_by = $request->userid;
		$data->created_at = date('Y-m-d H:i:s');
		$data->updated_at = date('Y-m-d H:i:s');
		$data->save();
		return response()->json($request->msg);
	}

	public function delChat(Request $request) {
		$data = Sychat::where('id', $request->id)->withTrashed()->first();
		$data->delete();

	}
}