<?php
namespace App\Http\Controllers\Sys;

use App\Http\Controllers\Controller;
use App\Model\Sys\Syrpt;
use App\Sf;
use Auth;
use DB;
use Illuminate\Http\Request;

class SyrptController extends Controller {

	public function index(Request $request) {
		if (!$plant = Sf::isPlant()) {
			return Sf::selectPlant();
		}

		Sf::log("sys_syrpt", "SyrptController@" . __FUNCTION__, "Open Page  ", "link");

		return view('sys.syrpt.syrpt_frm', compact(['request', 'plant']));
	}

	public function rpt01(Request $request) {
		if (!$plant = Sf::isPlant()) {
			return Sf::selectPlant();
		}

		Sf::log("sys_syrpt", "SyrptController@" . __FUNCTION__, "Open Page  ", "link");

		return view('sys.syrpt.syrpt_rpt01', compact(['request', 'plant']));
	}

	public function getList(Request $request) {
		if (!Sf::allowed('SYS_SYRPT_R')) {
			return response()->json(Sf::reason(), 401);
		}
		$data = Syrpt::where(function ($q) use ($request) {
			$q->orWhere('rptid', 'like', "%" . @$request->q . "%");
			$q->orWhere('ctg', 'like', "%" . @$request->q . "%");
			$q->orWhere('rptname', 'like', "%" . @$request->q . "%");
			$q->orWhere('rptdesc', 'like', "%" . @$request->q . "%");
		})
			->orderBy(isset($request->order_by) ? substr($request->order_by, 1) : 'rptid', substr(@$request->order_by, 0, 1) == '-' ? 'desc' : 'asc');
		if ($request->trash == 1) {
			$data = $data->onlyTrashed();
		}
		$data = $data->paginate(isset($request->limit) ? $request->limit : 10);
		return response()->json(compact(['data']));
	}

	public function getLookup(Request $request) {
		$data = Syrpt::where(function ($q) use ($request) {
			$q->orWhere('rptid', 'like', "%" . @$request->q . "%");
			$q->orWhere('ctg', 'like', "%" . @$request->q . "%");
			$q->orWhere('rptname', 'like', "%" . @$request->q . "%");
			$q->orWhere('rptdesc', 'like', "%" . @$request->q . "%");
			$q->orWhere('str_conn', 'like', "%" . @$request->q . "%");
			$q->orWhere('plant', 'like', "%" . @$request->q . "%");
		})
			->orderBy(isset($request->order_by) ? substr($request->order_by, 1) : 'rptid', substr(@$request->order_by, 0, 1) == '-' ? 'desc' : 'asc');
		$data = $data->paginate(isset($request->limit) ? $request->limit : 10);
		return view('sys.system.dialog.sflookup', compact(['data', 'request']));
	}

	public function store(Request $request) {
		$req = json_decode(request()->getContent());
		$h = $req->h;
		$f = $req->f;

		try {
			$arr = array_merge((array) $h, ['updated_at' => date('Y-m-d H:i:s')]);
			if ($f->crud == 'c') {
				if (!Sf::allowed('SYS_SYRPT_C')) {
					return response()->json(Sf::reason(), 401);
				}
				$data = new Syrpt();
				$arr = array_merge($arr, ['created_by' => Auth::user()->userid, 'created_at' => date('Y-m-d H:i:s')]);
				$data->create($arr);
				$id = DB::getPdo()->lastInsertId();
				Sf::log("sys_syrpt", $id, "Create Menu (syrpt) rptid : " . $id, "create");
				return response()->json('created');
			} else {
				if (!Sf::allowed('SYS_SYRPT_U')) {
					return response()->json(Sf::reason(), 401);
				}
				$data = Syrpt::find($h->rptid);
				$data->update($arr);
				$id = $data->rptid;
				Sf::log("sys_syrpt", $id, "Update Menu (syrpt) rptid : " . $id, "update");
				return response()->json('updated');
			}

		} catch (\Exception $e) {
			return response()->json($e->getMessage(), 500);
		}
	}

	public function edit($id) {
		$h = Syrpt::where('rptid', $id)->withTrashed()->first();
		return response()->json(compact(['h']));
	}

	public function destroy($id, Request $request) {
		try {
			$data = Syrpt::where('rptid', $id)->withTrashed()->first();
			if ($request->restore == 1) {
				if (!Sf::allowed('SYS_SYRPT_S')) {
					return response()->json(Sf::reason(), 401);
				}
				$data->restore();
				Sf::log("sys_syrpt", $id, "Restore Menu (syrpt) rptid : " . $id, "restore");
				return response()->json('restored');
			} else {
				if (!Sf::allowed('SYS_SYRPT_D')) {
					return response()->json(Sf::reason(), 401);
				}
				$data->delete();
				Sf::log("sys_syrpt", $id, "Delete Menu (syrpt) rptid : " . $id, "delete");
				return response()->json('deleted');
			}
		} catch (\Exception $e) {
			return response()->json($e->getMessage(), 500);
		}
	}

	public function simulate(Request $request) {
		$req = json_decode(request()->getContent());
		$h = $req->h;
		$f = $req->f;

		return $this->queryTable($h, $req->limit);
	}

	public function runTable(Request $request) {
		$h = Syrpt::find($request->id);
		return $this->queryTable($h, 0);
	}

	private function queryTable($h, $limit) {
		try {
			$data = DB::select(DB::raw($h->str_query . " " . ($limit == 0 ? '' : 'limit ' . $limit)));
			return response()->json(compact(['data']));
		} catch (\Exception $e) {
			return response()->json($e->getMessage(), 500);
		}
	}
}