<?php
namespace App\Http\Controllers\Sys;

use App\Http\Controllers\Controller;
use App\Model\Sys\Sydump;
use App\Sf;
use Auth;
use DB;
use File;
use Ifsnop\Mysqldump as IMysqldump;
use Illuminate\Http\Request;
use Storage;

class SydumpController extends Controller {

	public function index(Request $request) {
		if (!$plant = Sf::isPlant()) {
			return Sf::selectPlant();
		}

		Sf::log("sys_sydump", "SydumpController@" . __FUNCTION__, "Open Page  ", "link");

		return view('sys.sydump.sydump_frm', compact(['request', 'plant']));
	}

	public function getList(Request $request) {
		if (!Sf::allowed('SYS_SYDUMP_R')) {
			return response()->json(Sf::reason(), 401);
		}
		$data = Sydump::where(function ($q) use ($request) {
			$q->orWhere('id', 'like', "%" . @$request->q . "%");
			$q->orWhere('db_host', 'like', "%" . @$request->q . "%");
			$q->orWhere('db_name', 'like', "%" . @$request->q . "%");
			$q->orWhere('db_user', 'like', "%" . @$request->q . "%");
			$q->orWhere('db_pass', 'like', "%" . @$request->q . "%");
			$q->orWhere('config', 'like', "%" . @$request->q . "%");
			$q->orWhere('schedule_type', 'like', "%" . @$request->q . "%");
			$q->orWhere('schedule_time', 'like', "%" . @$request->q . "%");
			$q->orWhere('output_type', 'like', "%" . @$request->q . "%");
			$q->orWhere('output_path', 'like', "%" . @$request->q . "%");
			$q->orWhere('note', 'like', "%" . @$request->q . "%");
		})
			->orderBy(isset($request->order_by) ? substr($request->order_by, 1) : 'id', substr(@$request->order_by, 0, 1) == '-' ? 'desc' : 'asc');
		if ($request->trash == 1) {
			$data = $data->onlyTrashed();
		}
		$data = $data->paginate(isset($request->limit) ? $request->limit : 10);
		return response()->json(compact(['data']));
	}

	public function getLookup(Request $request) {
		$data = Sydump::where(function ($q) use ($request) {
			$q->orWhere('id', 'like', "%" . @$request->q . "%");
			$q->orWhere('db_host', 'like', "%" . @$request->q . "%");
			$q->orWhere('db_name', 'like', "%" . @$request->q . "%");
			$q->orWhere('db_user', 'like', "%" . @$request->q . "%");
			$q->orWhere('db_pass', 'like', "%" . @$request->q . "%");
			$q->orWhere('config', 'like', "%" . @$request->q . "%");
			$q->orWhere('schedule_type', 'like', "%" . @$request->q . "%");
			$q->orWhere('schedule_time', 'like', "%" . @$request->q . "%");
			$q->orWhere('output_type', 'like', "%" . @$request->q . "%");
			$q->orWhere('output_path', 'like', "%" . @$request->q . "%");
			$q->orWhere('note', 'like', "%" . @$request->q . "%");
		})
			->orderBy(isset($request->order_by) ? substr($request->order_by, 1) : 'id', substr(@$request->order_by, 0, 1) == '-' ? 'desc' : 'asc');
		$data = $data->paginate(isset($request->limit) ? $request->limit : 10);
		return view('sys.system.dialog.sflookup', compact(['data', 'request']));
	}

	public function store(Request $request) {
		$req = json_decode(request()->getContent());
		$h = $req->h;
		$f = $req->f;

		try {
			$arr = array_merge((array) $h, ['updated_at' => date('Y-m-d H:i:s')]);
			if ($f->crud == 'c') {
				if (!Sf::allowed('SYS_SYDUMP_C')) {
					return response()->json(Sf::reason(), 401);
				}
				$data = new Sydump();
				$arr = array_merge($arr, ['created_by' => Auth::user()->userid, 'created_at' => date('Y-m-d H:i:s')]);
				$data->create($arr);
				$id = DB::getPdo()->lastInsertId();
				Sf::log("sys_sydump", $id, "Create Menu (sydump) id : " . $id, "create");
				return response()->json('created');
			} else {
				if (!Sf::allowed('SYS_SYDUMP_U')) {
					return response()->json(Sf::reason(), 401);
				}
				$data = Sydump::find($h->id);
				$data->update($arr);
				$id = $data->id;
				Sf::log("sys_sydump", $id, "Update Menu (sydump) id : " . $id, "update");
				return response()->json('updated');
			}

		} catch (\Exception $e) {
			return response()->json($e->getMessage(), 500);
		}
	}

	public function edit($id) {
		$h = Sydump::where('id', $id)->withTrashed()->first();
		return response()->json(compact(['h']));
	}

	public function destroy($id, Request $request) {
		try {
			$data = Sydump::where('id', $id)->withTrashed()->first();
			if ($request->restore == 1) {
				if (!Sf::allowed('SYS_SYDUMP_S')) {
					return response()->json(Sf::reason(), 401);
				}
				$data->restore();
				Sf::log("sys_sydump", $id, "Restore Menu (sydump) id : " . $id, "restore");
				return response()->json('restored');
			} else {
				if (!Sf::allowed('SYS_SYDUMP_D')) {
					return response()->json(Sf::reason(), 401);
				}
				$data->delete();
				Sf::log("sys_sydump", $id, "Delete Menu (sydump) id : " . $id, "delete");
				return response()->json('deleted');
			}
		} catch (\Exception $e) {
			return response()->json($e->getMessage(), 500);
		}
	}

	public function backup(Request $request) {
		return $this->backupRun($request->id);
	}

	public function backupRun($id) {
		$sydump = Sydump::find($id);
		if ($sydump == false) {
			return response()->json("Master Data Not Found", 500);
		}

		if ($sydump->output_path == '') {
			return response()->json("Output Path cant be blank", 500);
		}
		try {
			set_time_limit(0);
			$dumpSettings = array(
				'compress' => IMysqldump\Mysqldump::GZIP,
				'default-character-set' => IMysqldump\Mysqldump::UTF8,
				'no-data' => false,
				'add-drop-table' => false,
				'single-transaction' => true,
				'lock-tables' => false,
				'add-locks' => false,
				'extended-insert' => true,
				'disable-foreign-keys-check' => true,
				'skip-triggers' => true,
				'add-drop-trigger' => true,
				'databases' => false,
				'add-drop-database' => false,
				'hex-blob' => true,
			);
			$dump = new IMysqldump\Mysqldump('mysql:host=' . $sydump->db_host . ';dbname=' . $sydump->db_name, $sydump->db_user, base64_decode($sydump->db_pass), $dumpSettings);
			set_time_limit(0);
			$filename = $sydump->output_path . '/' . $sydump->db_name . "-" . date("Y-m-d__H_i_s") . ".gzip";

			if ($sydump->output_type == 'ftp') {
				$filename = "mysqldump/" . $filename;

				if (!is_dir(dirname(storage_path("app/" . $filename))) && !File::makeDirectory(dirname(storage_path("app/" . $filename)), 0777, true)) {
					$str = "Error creating folder $filename";
					return response()->json($str, 500);
				}

				try {
					$dump->start(storage_path("app/" . $filename));
					$content = Storage::get($filename);
					Storage::disk('ftp')->put($filename, $content);
					Storage::delete($filename);
					Sf::log("sys_sydump", $id, "Run Backup Mysqldumpid: " . $id . " (" . $sydump->db_host . " - " . $sydump->db_name . ") ,output : $filename", "execute");
					return response()->json('Success. Destination : ' . $filename);
				} catch (\Exception $e) {
					$str = "FTP Failed. " . $e->getMessage();
					return response()->json($str, 500);
				}

			} else if ($sydump->output_type == 'local') {
				if (!is_dir(dirname($filename)) && !File::makeDirectory(dirname($filename), 0777, true)) {
					$str = "Error creating folder $filename";
					return response()->json($str, 500);
				}
				$dump->start($filename);
				return response()->json('Success. Destination : ' . $filename);
			}

		} catch (\Exception $e) {
			$str = 'Sorry, mysqldump-php error: ' . $e->getMessage();
			return response()->json($str, 500);
		}
	}
}